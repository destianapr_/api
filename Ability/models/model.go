package models

type Ability struct {
	IDAbility		int		`json:"id_ability"`
	IDKategori		int		`json:"id_kategori"`
	NamaAbility		string	`json:"nama"`
	ImgPath			string	`json:"img_path"`
	Total			int 	`json:"total"`
}

type Ability_db struct {
	IDAbility		int		`json:"id_ability"`
	IDKategori		int		`json:"id_kategori"`
	NamaAbility		string	`json:"nama"`
	ImgPath			string	`json:"img_path"`
}

type AbilityCMD_db struct{
	//IDAbilityCMD	int		`json:"id_ability_cmd"`
	IDAbility		int 	`json:"id_ability"`
	Command			string	`json:"command"`
}

type ResponseAbility struct {
	Status	int	`json:"status"`
	Message string	`json:"message"`
	Data 	Ability_db
}

type ResponseAbilityCMD struct {
	Status	int	`json:"status"`
	Message string	`json:"message"`
	Data 	AbilityCMD_db
}

type AmbilAbility struct {
	Status	int		`json:"status"`
	Message string	`json:"message"`
	Size 	int 	`json:"size"`
	Data 	interface{}	`json:"data"`
}

type AmbilAbilityCMD struct {
	Status	int		`json:"status"`
	Message string	`json:"message"`
	Data 	interface{}	`json:"data"`
}