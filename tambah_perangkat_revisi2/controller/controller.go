package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"SmartSpeaker/Api/database"
	"SmartSpeaker/Api/tambah_perangkat_revisi2/models"
)

func HomeLink(w http.ResponseWriter, r *http.Request) {
	var response models.Response

	response.Status = 1
	response.Message = "Hai, Selamat Datang!!"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//tambah perangkat
func TambahSmartSpeaker(w http.ResponseWriter, r *http.Request) {
	var response models.Response

	db := database.Connect()
	defer db.Close()

	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueMID := r.FormValue("machine_id")
	ValueDeviceName := r.FormValue("device_name")
	ValueKategori := 1
	ValueISA := 1
	result := database.InsertTambahSS(ValueMID, ValueAlamatSurel, ValueDeviceName)
	count, _ := result.RowsAffected()
	if count == 1 || count == 2 || count == 0 {

		cek, hitung := database.SelectTambahSS(ValueAlamatSurel, ValueMID)
		if cek == nil && hitung == 1 {
			result := database.UpdateTambahSS(ValueDeviceName, ValueKategori, ValueISA, ValueAlamatSurel, ValueMID)
			rowcekupd, _ := result.RowsAffected()
			if rowcekupd == 1 {
				log.Print("berhasil melakukan update")
				response.Status = 200
				response.Message = "Berhasil melakukan update"
			} else if rowcekupd == 0 {
				log.Print("data sama")
				response.Status = 300
				response.Message = "Data sama"
			} else {
				log.Print("Gagal melalukan update")
				response.Status = 400
				response.Message = "Gagal update"
				return
			}
		} else {
			//email dan id_device belum ada. Insert data baru.
			result := database.InsertUserDevice(ValueDeviceName, ValueKategori, ValueISA, ValueMID, ValueAlamatSurel)
			rowcekins, _ := result.RowsAffected()
			if rowcekins == 1 {
				log.Print("data berhasil di masukkan")
				response.Status = 200
				response.Message = "berhasil memasukkan data"
			} else {
				log.Print("data gagal dimasukkan")
				response.Status = 400
				response.Message = "gagal memasukkan data"
			}
		}
	} else {
		log.Println("Error")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//Tambah perangkat otentifikasi
func TambahOtentifikasi(w http.ResponseWriter, r *http.Request) {
	//id_skill = 1
	var response models.Response
	var hitung int

	db := database.Connect()
	defer db.Close()

	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValuePort := r.FormValue("port")
	ValueToken := r.FormValue("token")
	ValueUnique := r.FormValue("unique_id")
	ValueIP := r.FormValue("ip_address")
	ValueIDSkill := r.FormValue("id_skill")

	port, err := strconv.Atoi(ValuePort)
	if err != nil {
		fmt.Println("convert variabel gagal")
	}

	idskill, err := strconv.Atoi(ValueIDSkill)
	if err != nil {
		fmt.Println("convert variabel gagal")
	}

	//cek := db.QueryRow("SELECT count(email) as count_email FROM skill_auth_external WHERE email=? and unique_id=?", ValueAlamatSurel, ValueUnique).Scan(&hitung)
	cek, hitung := database.SelectTambahOtentifikasi(ValueAlamatSurel, ValueUnique)

	if cek == nil && hitung == 1 {
		//unique_id dan email sudah ada. Maka lakukan update pada token.
		//UPDATE skill_auth_external SET token=231096 WHERE email="edo08syahputra@gmail.com" and unique_id="qwerty"
		//result, err := db.Exec("UPDATE skill_auth_external SET token=? WHERE email=? and unique_id=?", ValueToken, ValueAlamatSurel, ValueUnique)
		result := database.UpdateTambahPerangkat(ValueToken, ValueAlamatSurel, ValueUnique)

		rowcekupd, _ := result.RowsAffected()

		if rowcekupd == 1 {
			log.Print("berhasil melakukan update")
			response.Status = 200
			response.Message = "Berhasil melakukan update"
		} else if rowcekupd == 0 {
			log.Print("Data sama.")
			response.Status = 300
			response.Message = "Data yang dimasukkan sama"
		} else {
			log.Print("gagal update")
			response.Status = 400
			response.Message = "Gagal melakukan update"
		}
	} else {
		//email dan unique_id belum ada. Insert data baru.
		//insert ke skill_auth_external
		//result, err := db.Exec("INSERT into skill_auth_external(id_skill, email, token, ip, port, unique_id) VALUES(?, ?, ?, ?, ?, ?)", ValueIDSkill, ValueAlamatSurel, ValueToken, ValueIP, ValuePort, ValueUnique)
		result := database.InsertTambahPerangkat(idskill, ValueAlamatSurel, ValueToken, ValueIP, port, ValueUnique)

		rowcekupd, _ := result.RowsAffected()

		if rowcekupd == 1 {
			log.Print("Data berhasil dimasukkan")
			response.Status = 200
			response.Message = "Berhasil memasukkan data"
		} else {
			log.Print("error")
			response.Status = 400
			response.Message = "Gagal memasukkan data"
		}

	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//Tambah perangkat
func TambahUserDevice(w http.ResponseWriter, r *http.Request) {
	//id_skill = 1
	var response models.Response
	var hitung int

	db := database.Connect()
	defer db.Close()

	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueUnique := r.FormValue("unique_id")
	ValueIDSkill := r.FormValue("id_skill")
	ValueNamaDevice := r.FormValue("device_name")
	ValueKategori := r.FormValue("kategori")

	kategori, err := strconv.Atoi(ValueKategori)
	if err != nil {
		fmt.Println("convert variabel gagal")
	}

	idskill, err := strconv.Atoi(ValueIDSkill)
	if err != nil {
		fmt.Println("convert variabel gagal")
	}

	//cek := db.QueryRow("SELECT count(email) as count_email FROM user_device WHERE email=? and id_device=?", ValueAlamatSurel, ValueUnique).Scan(&hitung)
	query, hitung := database.SelectUserDevice(ValueAlamatSurel, ValueUnique)

	if query == nil && hitung == 1 {
		//email dan id_device sudah ada. Lakukan update
		//result, _ := db.Exec("UPDATE user_device SET nama_device=?, id_kategori_device=?, id_skill_auth=? WHERE email=? and id_device=?", ValueNamaDevice, ValueKategori, ValueIDSkill, ValueAlamatSurel, ValueUnique)
		result := database.UpdateUserDevice(ValueNamaDevice, kategori, idskill, ValueAlamatSurel, ValueUnique)
		rowcekupd, _ := result.RowsAffected()

		if rowcekupd == 1 {
			log.Print("berhasil melakukan update")
			response.Status = 200
			response.Message = "Berhasil melakukan update"
		} else if rowcekupd == 0 {
			log.Print("Data sama.")
			response.Status = 300
			response.Message = "Data sama"
		} else {
			log.Print("gagal update")
			response.Status = 400
			response.Message = "Gagal memasukkan data"
			return
		}

	} else {
		//email dan id_device belum ada. Insert data baru.
		//err, _ := db.Exec("INSERT into user_device(nama_device, id_kategori_device, id_skill_auth, id_device, email) VALUES(?, ?, ?, ?, ?)", ValueNamaDevice, ValueKategori, ValueIDSkill, ValueUnique, ValueAlamatSurel)
		result := database.InsertUserDevice(ValueNamaDevice, kategori, idskill, ValueUnique, ValueAlamatSurel)

		rowcekins, _ := result.RowsAffected()

		if rowcekins == 1 {
			log.Print("data berhasil di masukkan")
			response.Status = 200
			response.Message = "Berhasil memasukkan data"
		} else {
			log.Print("data gagal dimasukkan")
			response.Status = 400
			response.Message = "Gagal memasukkan data"
		}

	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahPerangkat(w http.ResponseWriter, r *http.Request) { //ini bukan cuma untuk hue aja
	//id_skill = 1
	var response models.Response
	var hitung int

	db := database.Connect()
	defer db.Close()

	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueIP := r.FormValue("ip_address")
	ValuePort := r.FormValue("port")
	ValueToken := r.FormValue("token")
	ValueUnique := r.FormValue("unique_id")
	ValueIDSkill := 1
	ValueNamaDevice := r.FormValue("device_name")
	ValueKategori := 3

	port, err := strconv.Atoi(ValuePort)
	if err != nil {
		fmt.Println("convert variabel gagal")
	}

	//cek := db.QueryRow("SELECT count(email) as count_email FROM skill_auth_external WHERE email=? and unique_id=?", ValueAlamatSurel, ValueUnique).Scan(&hitung)

	cek, hitung := database.SelectTambahPerangkat(ValueAlamatSurel, ValueUnique)

	if cek == nil && hitung == 1 {
		//unique_id dan email sudah ada. Maka lakukan update pada token.
		//UPDATE skill_auth_external SET token=231096 WHERE email="edo08syahputra@gmail.com" and unique_id="qwerty"
		//err, _ := db.Exec("UPDATE skill_auth_external SET token=? WHERE email=? and unique_id=?", ValueToken, ValueAlamatSurel, ValueUnique)
		result := database.UpdateTambahPerangkat(ValueToken, ValueAlamatSurel, ValueUnique)
		rowcekupd, _ := result.RowsAffected()

		if rowcekupd == 1 {
			log.Print("berhasil melakukan update")
			response.Status = 200
			response.Message = "Berhasil melakukan update"
		} else if rowcekupd == 0 {
			log.Print("Data sama.")
			response.Status = 300
			response.Message = "Data sama"
		} else {
			log.Print("gagal update")
			response.Status = 400
			response.Message = "Gagal memasukkan data"
		}
	} else {
		//email dan unique_id belum ada. Insert data baru.

		//insert ke skill_auth_external
		//err, _ := db.Exec("INSERT into skill_auth_external(id_skill, email, token, ip, port, unique_id) VALUES(?, ?, ?, ?, ?, ?)", ValueIDSkill, ValueAlamatSurel, ValueToken, ValueIP, ValuePort, ValueUnique)
		result := database.InsertTambahPerangkat(ValueIDSkill, ValueAlamatSurel, ValueToken, ValueIP, port, ValueUnique)
		rowcekupd, _ := result.RowsAffected()

		if rowcekupd == 1 {
			//insert ke user_device
			//err := db.QueryRow("SELECT id_skill_auth_external FROM skill_auth_external where email=? and unique_id=?", ValueAlamatSurel, ValueUnique).Scan(&id)
			err, id := database.SelectTambahPerangkat2(ValueAlamatSurel, ValueUnique)
			if err != nil {
				panic(err)
			}
			//err2, _ := db.Exec("INSERT into user_device(nama_device, id_kategori_device, id_skill_auth, id_device, email) VALUES(?, ?, ?, ?, ?)", ValueNamaDevice, ValueKategori, id, ValueUnique, ValueAlamatSurel)
			result := database.InsertUserDevice(ValueNamaDevice, ValueKategori, id, ValueUnique, ValueAlamatSurel)
			rowcekins, _ := result.RowsAffected()

			if rowcekins == 1 {
				log.Print("data sudah di masukkan")
				response.Status = 200
				response.Message = "Berhasil memasukkan data"
			} else {
				log.Print("data gagal dimasukkan")
				response.Status = 400
				response.Message = "Gagal memasukkan data"
			}
		} else {
			log.Print("error")
		}

	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahAuthDevice(w http.ResponseWriter, r *http.Request) {
	var cek int
	var response models.ResponseData

	db := database.Connect()
	defer db.Close()

	IDSkill := r.FormValue("id_skill")
	Ip := r.FormValue("ip")
	Port := r.FormValue("port")
	Unique := r.FormValue("unique_id")
	AlamatSurel := r.FormValue("alamat_surel")
	Token := r.FormValue("token")

	query, cek := database.SelectCekAuth(AlamatSurel, IDSkill)
	switch {
	case query == nil && cek > 0:
		doUpdate := database.UpdateAuthDevice(IDSkill, AlamatSurel, Ip, Port, Unique, Token)
		rowcekupd, _ := doUpdate.RowsAffected()
		if rowcekupd == 1 {
			log.Print("berhasil melakukan update")
			response.Status = 200
			response.Message = "Berhasil melakukan update"
		} else if rowcekupd == 0 {
			log.Print("data sama")
			response.Status = 300
			response.Message = "Data sama"
		} else {
			log.Print("Gagal melalukan update")
			response.Status = 400
			response.Message = "Gagal update"
			return
		}
	default:
		doInsert := database.InserAuthDevice(IDSkill, AlamatSurel, Ip, Port, Unique, Token)
		switch {
		case doInsert == nil:
			log.Println("Status_Insert")
		}
		response.Status = 1
		response.Message = "Berhasil di tambahkan"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GetDeviceByEmail(w http.ResponseWriter, r *http.Request) {
	var cek int
	var response models.ResponseData
	var result models.DataDevice
	var arr_result []models.DataDevice
	db, _ := database.Connect2()
	defer db.Close()
	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueIDSkill := r.FormValue("id_skill")
	// query := db.QueryRow("Select count(email) as jumlah from user_device where email=? and id_skill=? ", ValueAlamatSurel, ValueIDSkill).Scan(&cek.Jumlah)
	query, cek := database.SelectDeviceByEmail(ValueAlamatSurel, ValueIDSkill)
	log.Println(cek)
	switch {
	case query == nil && cek > 0:
		rows, err := db.Query("SELECT COUNT(email) as jumlah, email as alamat_surel, nama_device as nama_device , id_device as id_device  from user_device where email=? and id_skill=? group by id_device", ValueAlamatSurel, ValueIDSkill)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer rows.Close()
		for rows.Next() {
			var err = rows.Scan(&result.Total, &result.AlamatSurel, &result.NamaDevice, &result.IdDevice)
			if err != nil {
				fmt.Println(err.Error())
				return
			} else {
				log.Println("dua , err == nil")
				arr_result = append(arr_result, result)
				if len(arr_result) < 1 {
					log.Println("data tidak ada")
				} else {
					response.Status = 1
					response.Message = "ada"
					log.Println("Data Ada")
					response.Data = arr_result
				}
			}
		}
	default:
		response.Status = 0
		response.Message = " Tidak Ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
