package models

import (
	"github.com/dgrijalva/jwt-go"
)

var jwtKey = []byte("kunci_bahasa_kita")
var errq error

type Users struct {
	AlamatSurel  string `form:"alamat_surel" json:"alamat_surel"`
	NamaPengguna string `form:"nama_pengguna" json:"nama_pengguna"`
	KataSandi    string `form:"kata_sandi" json:"kata_sandi"`
	UID          string `form:"uid" json:"uid"`
	Status       int    `form:"status" json:status`
	Otoritas     int    `form:"otoritas" json:"otoritas"`
	Jumlah       int    `form:"jumlah" json:"jumlah"`
}

type User_device struct {
	AlamatSurel  string `json:"alamat_sure"`
	MachineID    string `json:"machine_id"`
	NamaDevice   string `json:"nama_device"`
	Kategori     string `json:"kategori"`
	IDSkillAuth  string `json:"id_skill_auth"`
	IDUserDevice string `json:"id_user_device"`
}

type AuthDevice struct {
	IDSkill  int    `json:"id_skill"`
	IP       string `json:"ip"`
	Port     int    `json:"port"`
	UniqueID string `json:"unique_id"`
	Email    string `json:"email"`
	Token    string `json:"token"`
}

type PerangkatSS struct {
	Email     string `json:"email"`
	MachineID string `json:"machineid`
}

type Tag struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Uid      string `json:"uid"`
	Otoritas int    `json:"otoritas"`
}

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    Tag
}

type Claims struct {
	Pengguna []byte
	Surel    []byte
	Sandi    []byte
	jwt.StandardClaims
}

type DataDevice struct {
	Total       int    `json:"total"`
	AlamatSurel string `json:"alamat_sure"`
	NamaDevice  string `form:"nama_device" json:"nama_device"`
	IdDevice    string `form:"id_device" json:"id_device"`
}

type ResponseData struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:data`
}
