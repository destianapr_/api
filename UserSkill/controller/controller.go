package controller

import (
	"SmartSpeaker/Api/UserSkill/models"
	"SmartSpeaker/Api/database"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func TambahSkillBawaan(w http.ResponseWriter, r *http.Request) {
	var respon models.Response
	var Userskill models.UserSkill
	var Userskill_db models.UserSkill_db
	db := database.Connect()
	defer db.Close()
	Alamat_surel := r.FormValue("alamat_surel")
	query, Userskill, Userskill_db := database.SelectSkill(Alamat_surel)
	if query == nil && Userskill.Total > 0 {
		//jika data ada maka tampilkan data
		respon.Status = 0
		respon.Message = "Skill Bawaan Sudah Ada"
		respon.Data = Userskill_db
		log.Println("Skill Bawaan Sudah Ada")
	} else {
		//jika data belum ada maka lakukan Insert dengan 3 jenis id_skill
		for i := 1; i <= 3; i++ {
			//result := db.QueryRow("INSERT INTO user_skill(email, id_skill) Values(?,?)", Alamat_surel, i)
			result := database.InsertSkillBawaan(Alamat_surel, i)
			if result == nil {
				log.Println("id_skill", i, " Berhasil di masukkan")
			}
		}
		respon.Status = 1
		respon.Message = "Skill Bawaan Berhasil di Tambahkan"
		respon.Data = Userskill_db
		log.Println("Skill Bawaan Berhasil di Tambahkan")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(respon)
}

func Ambil(w http.ResponseWriter, r *http.Request) {
	var Userskill models.UserSkill
	var _ models.UserSkill_db
	var response models.ResponseInfoSkill
	db := database.Connect()
	defer db.Close()
	Alamat_surel := r.FormValue("alamat_surel")
	//query := db.QueryRow("SELECT COUNT(email) as total FROM  user_skill WHERE email=?",Alamat_surel).Scan(&Total)
	query, Userskill, _ := database.SelectSkill(Alamat_surel)

	if query == nil && Userskill.Total > 0 {
		var InfoSkill models.InfoSkill
		var Infoskill_db models.InfoSkill_db
		query2, InfoSkill, Infoskill_db := database.SelectAmbilInfoSkill(Alamat_surel)
		log.Println(InfoSkill.Total)
		if query2 == nil && InfoSkill.Total > 0 {
			log.Println("data terdetect")
			log.Println(Userskill.Total)
			response.Status = 1
			response.Message = "Skill " + Alamat_surel + "  di Temukan"
			response.Data = Infoskill_db
			log.Println("Skill di Temukan")
		}
	} else {
		response.Status = 0
		response.Message = "Skill " + Alamat_surel + "  Belum Ada"
		log.Println("Skill Belum Ada")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//
//func Ambil2(w http.ResponseWriter, r *http.Request){
//	var Userskill models.UserSkill
//	var _ models.UserSkill_db
//	var response models.Responseinfo
//	db,err := database.Connect3()
//	if err != nil {
//		fmt.Println(err.Error())
//		return
//	}
//	defer db.Close()
//	Alamat_surel := r.FormValue("alamat_surel")
//	//query := db.QueryRow("SELECT COUNT(email) as total FROM  user_skill WHERE email=?",Alamat_surel).Scan(&Total)
//	query, Userskill, _ := database.SelectSkill(Alamat_surel)
//
//	if query == nil && Userskill.Total > 0 {
//		var InfoSkill models.InfoSkill
//		var Infoskill_db models.InfoSkill_db
//		var Arr_infoskildb []models.InfoSkill_db
//		query2,err := db.Query("SELECT COUNT(email) as Total,  (COALESCE(skill.id_skill, '-')) AS id_skill, (COALESCE(skill_deskripsi.nama_pembuat, '-')) AS Nama_Pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) AS Logo_Skill, (COALESCE(skill_deskripsi.deskripsi, '-')) AS Deskripsi, (COALESCE(skill.nama_skill, '-')) AS Nama_Skill FROM user_skill, skill, skill_deskripsi WHERE user_skill.email= ? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill ", Alamat_surel)
//		if err != nil {
//			fmt.Println(err.Error())
//			return
//		}
//		//query2, InfoSkill, Infoskill_db := database.SelectAmbilInfoSkill(Alamat_surel)
//		//if query2 == nil && InfoSkill.Total > 0 {
//		//	log.Println("data terdetect")
//		//	log.Println(Userskill.Total)
//		//	response.Status = 1
//		//	response.Message = "Skill "+Alamat_surel+"  di Temukan"
//		//	response.Data = Infoskill_db
//		//	log.Println("Skill di Temukan")
//		//}
//		for query2.Next(){
//			var err = query2.Scan(&InfoSkill.Total,
//				&Infoskill_db.IDSkill,
//				&Infoskill_db.NamaPembuat,
//				&Infoskill_db.LogoSkill,
//				&Infoskill_db.Deskripsi,
//				&Infoskill_db.NamaSkill)
//			if err != nil {
//				fmt.Println(err.Error())
//				return
//			} else{
//				log.Println("dua , err == nil")
//				Arr_infoskildb = append(Arr_infoskildb, Infoskill_db)
//				if len(Arr_infoskildb) < 1 {
//					log.Println("data tidak ada")
//				} else {
//					response.Status = 200
//					response.Message = "ada"
//					log.Println("Data Ada")
//					//
//					//for _, each = range result {
//					//	response.Data = result
//					//}
//					response.Data = Arr_infoskildb
//				}
//			}
//		}
//	}else {
//		response.Status = 0
//		response.Message = "Skill "+Alamat_surel+"  Belum Ada"
//		log.Println("Skill Belum Ada")
//	}
//	w.Header().Set("Content-Type", "application/json")
//	json.NewEncoder(w).Encode(response)
//}

func Ambil2(w http.ResponseWriter, r *http.Request) {
	var response models.Responseinfo
	var userskill models.UserSkill
	var userskill_db models.InfoSkill_db
	var Arruserskill_db []models.InfoSkill_db
	db, err := database.Connect2()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer db.Close()
	AlamatSurel := r.FormValue("alamat_surel")
	query := db.QueryRow("Select count(email) as total from user_skill where email=?", AlamatSurel).Scan(&userskill.Total)
	log.Println(userskill.Total)
	if query == nil && userskill.Total > 0 {
		//row, err:= db.Query("SELECT COUNT(email) as Total,(COALESCE(skill.id_skill, '-')) AS id_skill, (COALESCE(skill_deskripsi.nama_pembuat, '-')) AS Nama_Pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) AS Logo_Skill, (COALESCE(skill_deskripsi.deskripsi, '-')) AS Deskripsi, (COALESCE(skill.nama_skill, '-')) AS Nama_Skill FROM user_skill, skill, skill_deskripsi WHERE user_skill.email= ? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill ", AlamatSurel)
		row, err := db.Query("SELECT COUNT(email) as Total, (COALESCE(skill.id_skill)) as id_skill, (COALESCE(skill_deskripsi.nama_pembuat)) as nama_pembuat, (COALESCE(skill_deskripsi.logo_skill)) as logo_skill,(COALESCE(skill_deskripsi.deskripsi)) as deskripsi, (COALESCE(skill.nama_skill)) as nama_skill FROM user_skill, skill, skill_deskripsi WHERE user_skill.email=? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill group by id_skill", AlamatSurel)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		for row.Next() {
			var err = row.Scan(&userskill.Total, &userskill_db.IDSkill, &userskill_db.NamaPembuat, &userskill_db.LogoSkill, &userskill_db.Deskripsi, &userskill_db.NamaSkill)
			if err != nil {
				fmt.Println(err.Error())
				return
			} else {
				Arruserskill_db = append(Arruserskill_db, userskill_db)
				if len(Arruserskill_db) < 1 {
					log.Println("0")
				} else {
					response.Status = 1
					response.Message = "Show user_skill " + AlamatSurel
					response.Data = Arruserskill_db
				}
			}
		}
	} else {
		response.Status = 0
		response.Message = "user_skill " + AlamatSurel + " tidak ada"
		response.Data = Arruserskill_db
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Cari(w http.ResponseWriter, r *http.Request) {
	var Infoskill_db models.InfoSkill_db
	var ArrInfoskill_db []models.InfoSkill_db
	var Infoskill models.InfoSkill
	var response models.Responseinfo
	db := database.Connect()
	defer db.Close()
	Nama_Skill := r.FormValue("nama_skill")
	query, Infoskill, Infoskill_db := database.SelectNama_skill(Nama_Skill)
	log.Println(Infoskill.Total)
	if query == nil && Infoskill.Total > 0 {
		row, err := db.Query("Select COUNT(skill.nama_skill) as total, (COALESCE(skill_deskripsi.nama_pembuat, '-')) as nama_pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) as logo_skill, (COALESCE(skill_deskripsi.deskripsi, '-')) as deskripsi, (COALESCE(skill.nama_skill, '-')) as nama_skill, (COALESCE(user_skill.id_skill, 0 )) as id_skill from skill, skill_deskripsi, user_skill where skill.nama_skill LIKE ? and user_skill.id_skill=skill.id_skill and skill.id_skill=skill_deskripsi.id_skill group by id_user_skill", "%"+Nama_Skill+"%")
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		for row.Next() {
			var err = row.Scan(
				&Infoskill.Total,
				&Infoskill_db.NamaPembuat,
				&Infoskill_db.LogoSkill,
				&Infoskill_db.Deskripsi,
				&Infoskill_db.NamaSkill,
				&Infoskill_db.IDSkill)
			if err != nil {
				fmt.Println(err.Error())
				return
			} else {
				ArrInfoskill_db = append(ArrInfoskill_db, Infoskill_db)
				if len(ArrInfoskill_db) < 1 {
					log.Println("0")
				} else {
					response.Status = 1
					response.Message = "nama_skill " + Nama_Skill + " di temukan"
					response.Data = ArrInfoskill_db
				}
			}
		}

		//response.Status = 1
		//response.Message = "nama_skill ditemukan"
		//response.Data = ArrInfoskill_db
	} else {
		response.Status = 0
		response.Message = "nama_skill tidak ditemukan"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Cek(w http.ResponseWriter, r *http.Request) {
	var cek models.Cek
	var response models.ResponseCek
	db := database.Connect()
	defer db.Close()
	Alamat_surel := r.FormValue("alamat_surel")
	IDSkill := r.FormValue("id_skill")
	//query := db.QueryRow("select count(id_skill) as total from  user_skill where email=? and id_skill=?",Alamat_surel, IDSkill).Scan(&cek.Count)
	query, cek := database.SelectCek(Alamat_surel, IDSkill)
	if query == nil && cek.Count > 0 {
		response.Status = 1
		response.Message = "Skill " + Alamat_surel + " Ada"
		response.Data = cek
		log.Println("Skill Ada")
	} else {
		response.Status = 0
		response.Message = "Skill " + Alamat_surel + " tidak Ada"
		log.Println("Skill tidak Ada")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Hapus(w http.ResponseWriter, r *http.Request) {
	db := database.Connect()
	defer db.Close()
	var response models.Response
	var cek models.Cek
	Alamat_Surel := r.FormValue("alamat_surel")
	IDSkill := r.FormValue("id_skill")
	//cekidskill := db.QueryRow("SELECT COUNT(email) from user_skill where id_skill= ? and email = ?",IDSkill,Alamat_Surel).Scan(&Total)
	cekidskill, cek := database.SelectCek(Alamat_Surel, IDSkill)
	if cekidskill == nil && cek.Count > 0 {
		//lakukan delete
		rows := database.Hapus(Alamat_Surel, IDSkill)
		rowcekdelete, _ := rows.RowsAffected()
		if rowcekdelete == 0 {
			log.Println("ini row cek")
		}
		response.Status = 1
		response.Message = "Skill " + IDSkill + " pada email " + Alamat_Surel + " Berhasil di Hapus"
	} else {
		response.Status = 0
		response.Message = "Skill Tidak di temukan  "
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Tambah(w http.ResponseWriter, r *http.Request) {
	var Userskill models.UserSkill
	var respon models.Response
	var Userskill_db models.UserSkill_db
	db := database.Connect()
	defer db.Close()
	Alamat_surel := r.FormValue("alamat_surel")
	Id_skill := r.FormValue("id_skill")
	query := db.QueryRow("SELECT COUNT(email) as total, (COALESCE(email, '-')) as alamat_surel, (COALESCE(id_skill, 0)) as id_skill from  user_skill where email= ? and id_skill=?", Alamat_surel, Id_skill).
		Scan(
			&Userskill.Total,
			&Userskill_db.AlamatSurel,
			&Userskill_db.IDSkill)
	log.Println(Userskill.Total)
	if query == nil && Userskill.Total > 0 {
		//jika data ada maka tampilkan data
		respon.Status = 0
		respon.Message = "Skill Sudah Ada"
		respon.Data = Userskill_db
		log.Println("Skill Sudah Ada")
	} else {
		result := database.InsertSkillBaru(Alamat_surel, Id_skill)
		if result != nil {
			log.Println("data berhasil ditambah")
		}
		respon.Status = 1
		respon.Message = "Skill Berhasil di Tambahkan"
		log.Println("Skill  Berhasil di Tambahkan")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(respon)
}
