package models

type UserSkill struct{
	AlamatSurel string `form:"alamat_surel" json:"alamat_surel"`
	IDSkill 	string `form:"id_skill" json:"id_skill"`
	Total		int `from:"Total" json:"Total"`
}

type UserSkill_db struct{
	AlamatSurel string `form:"alamat_surel" json:"alamat_surel"`
	IDSkill 	string `form:"id_skill" json:"id_skill"`
}

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    UserSkill_db
}

//coba untuk append
//type Response2 struct {
//	Status  int    `json:"status"`
//	Message string `json:"message"`
//	Data    interface{} `json:"data"`
//}

type InfoSkill struct{
	IDSkill		string	`form:"id_skill" json:"id_skill"`
	NamaPembuat string 	`form:"nama_pembuat" json:"nama_pembuat"`
	LogoSkill	string 	`form:"logo_skill" json:"logo_skill"`
	Deskripsi	string 	`form:"deskripsi" json:"deskripsi"`
	NamaSkill	string 	`form:"nama_skill" json:"nama_skill"`
	Total 		int 	`form:"total" json:"total`
}

type InfoSkill_db struct{
	IDSkill		string		`form:"id_skill" json:"id_skill"`
	NamaPembuat string 	`form:"nama_pembuat" json:"nama_pembuat"`
	LogoSkill	string 	`form:"logo_skill" json:"logo_skill"`
	Deskripsi	string 	`form:"deskripsi" json:"deskripsi"`
	NamaSkill	string 	`form:"nama_skill" json:"nama_skill"`
}

type ResponseInfoSkill struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    InfoSkill_db
}

type Cek struct{
	Count int `json:"Count"`
}

type ResponseCek struct{
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    Cek
}

type Responseinfo struct {
	Status 	int				`json:"status"`
	Message string			`json:"message"`
	Data 	interface{}		`json:"data"`
}
