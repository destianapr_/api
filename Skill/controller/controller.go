package controller

import (
	"SmartSpeaker/Api/Skill/models"
	"SmartSpeaker/Api/database"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func Tambah(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill
	var Skill_db models.Skill_db
	db := database.Connect()
	defer db.Close()
	NamaSkill := r.FormValue("nama_skill")
	IDKategori := r.FormValue("id_kategori_skill")

	// query := db.QueryRow("Select Count(nama_skill) as total, (coalesce(id_kategori_skill, 0)) as iDKategory, (coalesce(nama_skill, '-')) as nama_skill, (coalesce(status_skill, 0)) as status_skill from skill where nama_skill=? && id_kategori_skill=?", NamaSkill, IDKategori).
	// 	Scan(&Skill.Total,
	// 		&Skill_db.IDKategoriSkill,
	// 		&Skill_db.NamaSkill,
	// 		&Skill_db.StatusSkill,
	// 	)
	query, Skill, Skill_db := database.SelectSkillbyNamadanIDaKategori(NamaSkill, IDKategori)
	switch {
	case query == nil && Skill.Total > 0:
		response.Status = 0
		response.Message = "Skill sudah Ada"
		response.Data = Skill_db
	default:
		//row, err := db.Exec("Insert into skill(nama_skill,id_kategori_skill) values (?,?)", NamaSkill, IDKategori)
		row := database.InsertTambahSkill(NamaSkill, IDKategori)
		switch {
		case row == nil:
			log.Print("INSERT")
		default:
			response.Status = 1
			response.Message = "Skill berhasil ditambahkan"
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahSkillCommand(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	db := database.Connect()
	defer db.Close()
	ID_skill := r.FormValue("id_skill")
	Pertanyaaan := r.FormValue("pertanyaan")
	Jawaban := r.FormValue("jawaban")
	Topic := r.FormValue("topic")
	//row, err := db.Exec("Insert into skill_command(id_skill, pertanyaan, jawaban, topic) values (?,?,?,?)", ID_skill, Pertanyaaan, Jawaban, Topic)
	row := database.InsertSkillCommand(ID_skill, Pertanyaaan, Jawaban, Topic)
	switch {
	case row == nil:
		log.Print("Insert data")
	default:
		response.Status = 1
		response.Message = "Skill Command berhasil ditambahkan"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahSkillDeskripsi(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill
	var Skilldes_db models.SkillDes_db
	db := database.Connect()
	defer db.Close()
	id_skill := r.FormValue("id_skill")
	nama_pembuat := r.FormValue("nama_pembuat")
	logo_skill := r.FormValue("logo_skill")
	deskripsi := r.FormValue("deskripsi")
	auth := r.FormValue("auth")
	discover := r.FormValue("discover")
	// query := db.QueryRow("Select Count(id_skill) as total, (coalesce(id_skill, 0)) as iDSkill, (coalesce(nama_pembuat,'-')) as nama_pembuat, (coalesce(logo_skill,'-')) as logo_skill, (coalesce(deskripsi,'-')) as Deskripsi, (coalesce(auth,'-')) as Auth, (coalesce(discover,'-')) as Discover from skill_deskripsi where id_skill=? ", id_skill).
	// 	Scan(
	// 		&Skill.Total,
	// 		&Skilldes_db.IDSkill,
	// 		&Skilldes_db.NamaPembuat,
	// 		&Skilldes_db.LogoSkill,
	// 		&Skilldes_db.Deskripsi,
	// 		&Skilldes_db.Auth,
	// 		&Skilldes_db.Discover)
	query, Skill := database.SelectSkillDeskripsi(id_skill)
	log.Println(Skill.Total)
	switch {
	case query == nil && Skill.Total > 0:
		response.Status = 0
		response.Message = "Skill Deskripsi Telah Ada"
		response.Data = Skilldes_db
	default:
		//row, err := db.Exec("Insert into skill_deskripsi(id_skill,nama_pembuat,logo_skill,deskripsi,auth,discover) values(?,?,?,?,?,?)", id_skill, nama_pembuat, logo_skill, deskripsi, auth, discover)
		row := database.InsertSkillDeskripsi(id_skill, nama_pembuat, logo_skill, deskripsi, auth, discover)
		switch {
		case row == nil:
			log.Println("data sama")
		default:
			response.Status = 1
			response.Message = "Skill Deskripsi Berhasil Ditambahkan"
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahSkillKategori(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill
	db := database.Connect()
	defer db.Close()
	NamaKategori := r.FormValue("nama_kategori")
	//query := db.QueryRow("Select count(nama_kategori) as total from skill_kategori where nama_kategori=? ", NamaKategori).Scan(&Skill.Total)
	query, Skill := database.SelectSkillKategori(NamaKategori)
	log.Println(Skill.Total)
	switch {
	case query == nil && Skill.Total > 0:
		response.Status = 0
		response.Message = "Nama Kategori Telah Ada"
	default:
		// row, err := db.Exec("Insert Into skill_kategori(nama_kategori) values(?)", NamaKategori)
		row := database.InsertSkillKategori(NamaKategori)
		switch {
		case row == nil:
			log.Println("data ada")
		default:
			response.Status = 1
			response.Message = "Kategori Baru Berhasil di tambahkan"
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahSkillReview(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	db := database.Connect()
	defer db.Close()
	ID_skill := r.FormValue("id_skill")
	Review := r.FormValue("review")
	Rating := r.FormValue("rating")
	EmailReviewer := r.FormValue("email_reviewer")

	// row, err := db.Exec("Insert into skill_review(id_skill, review, rating, email_reviewer) values (?,?,?,?)", ID_skill, Review, Rating, EmailReviewer)
	row := database.InsertSkillReview(ID_skill, Review, Rating, EmailReviewer)
	switch {
	case row == nil:
		log.Print("Data Ada")
	default:
		response.Status = 1
		response.Message = "Skill Review berhasil ditambahkan"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Ambil(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill
	var Skill_db models.SkillDes_db
	var ArrSkill_db []models.SkillDes_db
	db, _ := database.Connect2()
	defer db.Close()
	StatusSkill := r.FormValue("status_skill")

	//query := db.QueryRow("SELECT count(id_skill) as total from skill where status_skill=?", StatusSkill).Scan(&Skill.Total)
	query, Skill := database.SelectStatusSkill(StatusSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//SELECT skill.id_skill, nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=0 and skill.id_skill=skill_deskripsi.id_skill
		//row, _ := db.Query("SELECT (coalesce(id_skill, 0))as id_skill, (coalesce(nama_pembuat,'-'))as nama_pembuat, (coalesce(logo_skill,'-'))as logo_skill,(coalesce(deskripsi,'-'))as deskripsi,(coalesce(nama_skill))as nama_skill from skill,skill_deskripsi where status_skill=? and  skill.id_skill=skill_deskripsi.id_skill",StatusSkill)
		// row, _ := db.Query("SELECT nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=? and skill.id_skill=skill_deskripsi.id_skill", StatusSkill)
		row := database.SelectDatabyStatusSkill(StatusSkill)
		//	fmt.Println(reflect.TypeOf(row))
		for row.Next() {
			var err = row.Scan(&Skill_db.NamaPembuat, &Skill_db.LogoSkill, &Skill_db.Deskripsi, &Skill_db.NamaSkill)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrSkill_db = append(ArrSkill_db, Skill_db)
			}
		}
		response.Status = 1
		response.Message = "id_skill ada dengan Status skill = " + StatusSkill
		response.Data = ArrSkill_db
	default:
		response.Status = 0
		response.Message = "id_skill tidak ada"
		response.Data = ArrSkill_db
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilByKategori(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill
	var Skill_db models.SkillDes_db
	var ArrSkill_db []models.SkillDes_db
	db, _ := database.Connect2()
	defer db.Close()
	StatusSkill := r.FormValue("status_skill")
	IDKategori := r.FormValue("id_kategori_skill")
	// query := db.QueryRow("SELECT count(id_skill) as total from skill where status_skill=? and id_kategori_skill=?", StatusSkill, IDKategori).Scan(&Skill.Total)
	query, Skill := database.SelectIDKategori(StatusSkill, IDKategori)
	log.Print(Skill.Total)
	switch {
	case query == nil && Skill.Total > 0:
		//SELECT skill.id_skill, nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=0 and skill.id_skill=skill_deskripsi.id_skill
		//row, _ := db.Query("SELECT (coalesce(id_skill, 0))as id_skill, (coalesce(nama_pembuat,'-'))as nama_pembuat, (coalesce(logo_skill,'-'))as logo_skill,(coalesce(deskripsi,'-'))as deskripsi,(coalesce(nama_skill))as nama_skill from skill,skill_deskripsi where status_skill=? and  skill.id_skill=skill_deskripsi.id_skill",StatusSkill)
		//row, _ := db.Query("SELECT nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=? and id_kategori_skill=? and skill.id_skill=skill_deskripsi.id_skill", StatusSkill, IDKategori)
		row := database.SelectDatabyKategori(StatusSkill, IDKategori)
		for row.Next() {
			var err = row.Scan(&Skill_db.NamaPembuat, &Skill_db.LogoSkill, &Skill_db.Deskripsi, &Skill_db.NamaSkill)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrSkill_db = append(ArrSkill_db, Skill_db)
				response.Status = 1
				response.Message = "Status skill = " + StatusSkill
				response.Data = ArrSkill_db
			}
		}
		response.Status = 1
		response.Message = "id_skill ada dengan Status skill = " + StatusSkill
	default:
		response.Status = 0
		response.Message = "Skill by Kategori " + IDKategori + " tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAuthByEmail(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Skill models.Skill
	var SkillAuth models.SkillAuth
	var ArrSkillAuth []models.SkillAuth
	db, _ := database.Connect2()
	defer db.Close()
	Alamat_surel := r.FormValue("alamat_surel")
	IDSkill := r.FormValue("id_skill")
	//query := db.QueryRow("SELECT count(email) as total from skill_auth_external where email=? and id_skill=?", Alamat_surel, IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectAuthByEmail(Alamat_surel, IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//row, _ := db.Query("SELECT ip, port, unique_id,token from skill_auth_external where emaiL=? and id_skill=? ", Alamat_surel, IDSkill)
		row := database.SelectDataAuthByEmail(Alamat_surel, IDSkill)
		for row.Next() {
			var err = row.Scan(&SkillAuth.IP, &SkillAuth.Port, &SkillAuth.Unique_id, &SkillAuth.Token)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrSkillAuth = append(ArrSkillAuth, SkillAuth)
				response.Status = 1
				response.Size = Skill.Total
				response.Message = "Skill By Email ada"
				response.Data = ArrSkillAuth
			}
		}
	default:
		response.Status = 0
		response.Size = 0
		response.Message = " Skill By Email tidak ada "
		response.Data = SkillAuth
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAuthBySS(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Skill models.Skill2

	var SkillAuth models.SkillAuth
	var ArrSkillAuth []models.SkillAuth
	db, _ := database.Connect2()
	defer db.Close()
	IDSSDevice := r.FormValue("id_ss_device")
	//AlamatSurel := r.FormValue("Alamat_surel")
	IDSkill := r.FormValue("id_skill")
	//query := db.QueryRow("Select count(email) as total, email from ss_device where id_ss_device=?", IDSSDevice).Scan(&Skill.Total, &Skill.AlamatSurel)
	query, Skill := database.SelectAuthBySS(IDSSDevice)
	log.Println(Skill.Total)
	switch {
	case query == nil && Skill.Total > 0:
		var Skill2 models.Skill
		//cek := db.QueryRow("Select count(email) as total from  skill_auth_external where email = ? and id_skill=?", Skill.AlamatSurel, IDSkill).Scan(&Skill2.Total)
		cek, Skill2 := database.SelectCekAuthBySS(Skill.AlamatSurel, IDSkill)
		log.Println("cek: ", Skill2.Total)
		switch {
		case cek == nil && Skill2.Total > 0:
			//row, _ := db.Query("select ip,port,unique_id,token from skill_auth_external where email=? and id_skill=?", Skill.AlamatSurel, IDSkill)
			row := database.SelectDataAuthBySS(Skill.AlamatSurel, IDSkill)
			for row.Next() {
				var err = row.Scan(&SkillAuth.IP, &SkillAuth.Port, &SkillAuth.Unique_id, &SkillAuth.Token)
				switch {
				case err != nil:
					fmt.Println(err.Error())
					return
				default:
					ArrSkillAuth = append(ArrSkillAuth, SkillAuth)
					response.Status = 1
					response.Message = "ada"
					response.Size = Skill2.Total
					response.Data = ArrSkillAuth
				}
			}
		default:
			response.Status = 0
			response.Message = "id_skill tidak ada"
			response.Size = Skill2.Total
			response.Data = ArrSkillAuth
		}
	default:
		response.Status = 0
		response.Message = "Device tidak ada"
		response.Size = Skill.Total
		response.Data = ArrSkillAuth
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAuthSkill(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Auth1 models.SkillDes_db
	var ArrAuth []models.SkillDes_db
	var Skill models.Skill
	db, _ := database.Connect2()
	defer db.Close()
	IDSkill := r.FormValue("id_skill")
	// query := db.QueryRow("select count(id_skill) as total from  skill_deskripsi where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectAuthSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//row, _ := db.Query("Select auth from skill_deskripsi where id_skill=?", IDSkill)
		row := database.SelectDataAuthSkill(IDSkill)
		for row.Next() {
			var err = row.Scan(&Auth1.Auth)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrAuth = append(ArrAuth, Auth1)
				response.Status = 1
				response.Message = "Auth ada"
				response.Size = Skill.Total
				response.Data = ArrAuth
			}
		}
	default:
		response.Status = 0
		response.Message = "Auth Tidak Ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilDiscoverSkill(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Discover models.SkillDes_db
	var ArrDiscover []models.SkillDes_db
	var Skill models.Skill
	db, _ := database.Connect2()
	defer db.Close()
	IDSkill := r.FormValue("id_skill")
	//query := db.QueryRow("select count(id_skill) as total from  skill_deskripsi where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectDiscoverSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//row, _ := db.Query("Select discover from skill_deskripsi where id_skill=?", IDSkill)
		row := database.SelectDataDiscoverSkill(IDSkill)
		for row.Next() {
			var err = row.Scan(&Discover.Discover)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrDiscover = append(ArrDiscover, Discover)
				response.Status = 1
				response.Message = "Discover ada"
				response.Size = Skill.Total
				response.Data = ArrDiscover
			}
		}
	default:
		response.Status = 0
		response.Message = "Discover Tidak Ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilSkillKategori(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Kategori models.Kategori
	var ArrKategori []models.Kategori
	var Skill models.Skill
	db, _ := database.Connect2()
	defer db.Close()

	query, Skill := database.SelectKategoriSkill()
	log.Println(Skill.Total)
	switch {
	case query == nil && Skill.Total > 0:
		row := database.SelectDataKategoriSkill()
		for row.Next() {
			var err = row.Scan(&Skill.Total, &Kategori.IDKategori, &Kategori.NamaKategori)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrKategori = append(ArrKategori, Kategori)
				response.Status = 1
				response.Message = "Skill Kategori ada"
				response.Size = Skill.Total
				response.Data = ArrKategori
			}
		}
	default:
		response.Status = 0
		response.Message = "Skill Kategori tidak ada"
		response.Size = Skill.Total
		response.Data = ArrKategori
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilReviewSkill(w http.ResponseWriter, r *http.Request) {
	db, _ := database.Connect2()
	defer db.Close()
	var Skill models.Skill
	var Review models.ReviewSkill
	var ArrReview []models.ReviewSkill
	var response models.ResponseSizeSkill

	IDSkill := r.FormValue("id_skill")
	Size := r.FormValue("size")
	var Size2, _ = strconv.Atoi(Size)
	//query := db.QueryRow("select count(review) as total from skill_review where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectReviewSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//row, _ := db.Query("select count(review) as total, review,rating,email_reviewer from skill_review where id_skill=? limit ?", IDSkill, Size2)
		row := database.SelectDataReviewSkill(IDSkill, Size2)
		for row.Next() {
			var err = row.Scan(&Skill.Total, &Review.Review, &Review.Rating, &Review.EmailReviewer)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrReview = append(ArrReview, Review)
				response.Status = 1
				response.Size = Skill.Total
				response.Message = "Review ada"
				response.Data = ArrReview
			}
		}
	default:
		response.Status = 0
		response.Size = Skill.Total
		response.Message = "Review tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilRatingSkill(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Skill models.Skill
	var Stotal models.STotal
	var ArrStotal []models.STotal
	db, _ := database.Connect2()
	defer db.Close()
	IDSkill := r.FormValue("id_skill")
	//query := db.QueryRow("Select count(id_skill) as total from skill_review where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectRatingSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//row, _ := db.Query("SELECT (sum(rating) div count(rating)) as total from skill_review where id_skill=?", IDSkill)
		row := database.SelectDataRatingSkill(IDSkill)
		for row.Next() {
			var err = row.Scan(&Stotal.Total)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrStotal = append(ArrStotal, Stotal)
				response.Status = 1
				response.Size = Skill.Total
				response.Message = "id_skill ada"
				response.Data = ArrStotal
			}
		}
	default:
		response.Status = 0
		response.Size = Skill.Total
		response.Message = "id_skill tidak ada"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilCommandSkill(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Skill models.Skill
	var CmdSkill models.CommandSkill
	var ArrCmdSkill []models.CommandSkill
	db, _ := database.Connect2()
	defer db.Close()
	IDSkill := r.FormValue("id_skill")
	Size := r.FormValue("size")
	var Size2, _ = strconv.Atoi(Size)
	// query := db.QueryRow("Select count(id_skill) as total from skill_command where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectCommandSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		// row, _ := db.Query("SELECT pertanyaan,jawaban,topic from skill_command where id_skill=? limit ?", IDSkill, Size2)
		row := database.SelectDataCommandSkill(IDSkill, Size2)
		for row.Next() {
			var err = row.Scan(&CmdSkill.Pertanyaan, &CmdSkill.Jawaban, &CmdSkill.Topic)
			switch {
			case err != nil:
				fmt.Println(err.Error())
				return
			default:
				ArrCmdSkill = append(ArrCmdSkill, CmdSkill)
				response.Status = 1
				response.Size = Skill.Total
				response.Message = "id_skill ada"
				response.Data = ArrCmdSkill
			}
		}
	default:
		response.Status = 0
		response.Size = Skill.Total
		response.Message = "id_skill tidak ada"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilCommandSkillbyTopic(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var Skill models.Skill

	var CmdSkill models.CommandSkill
	var ArrCmdSkill []models.CommandSkill
	db, _ := database.Connect2()
	defer db.Close()
	IDSkill := r.FormValue("id_skill")
	Topic := r.FormValue("topic")

	//query := db.QueryRow("Select count(id_skill) as total from skill_command where id_skill=?", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectCommandSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		//query2 := db.QueryRow("Select count(topic) as total from skill_command where topic=?", Topic).Scan(&Skill2.Total)
		var Skill2 models.Skill
		query2, Skill2 := database.SelectTopic(Topic, IDSkill)
		switch {
		case query2 == nil && Skill2.Total > 0:
			//row, _ := db.Query("SELECT pertanyaan,jawaban,topic from skill_command where topic=? && id_skill=?", Topic, IDSkill)
			row := database.SelectDatabyTopicSkill(Topic, IDSkill)
			for row.Next() {
				var err = row.Scan(&CmdSkill.Pertanyaan, &CmdSkill.Jawaban, &CmdSkill.Topic)
				switch {
				case err != nil:
					fmt.Println(err.Error())
					return
				default:
					ArrCmdSkill = append(ArrCmdSkill, CmdSkill)
					response.Status = 1
					response.Size = Skill.Total
					response.Message = "id_skill ada, topik ada"
					response.Data = ArrCmdSkill
				}
			}
		default:
			response.Status = 1
			response.Size = Skill2.Total
			response.Message = "id_skill ada, topik tidak ada"
			response.Data = ArrCmdSkill
		}
	default:
		response.Status = 0
		response.Size = Skill.Total
		response.Message = "id_skill tidak ada"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilSkillByNama(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSizeSkill
	var SkillDes models.SkillDes_db
	var ArrSkillDes []models.SkillDes_db
	db, _ := database.Connect2()
	defer db.Close()

	NamaSkill := r.FormValue("nama_skill")
	IDKategori := r.FormValue("id_kategori_skill")
	switch {
	case IDKategori == "-":
		var Skill models.Skill

		// query := db.QueryRow("Select Count(skill.nama_skill)as total from skill, skill_deskripsi where skill_deskripsi.id_skill=skill.id_skill and skill.nama_skill like ?", "%"+NamaSkill+"%").
		// Scan(&Skill.Total)
		query, Skill := database.SelectSkillbyNama1(NamaSkill)
		log.Println(Skill.Total)
		switch {
		case query == nil && Skill.Total > 0:
			//row, _ := db.Query("SELECT skill.nama_skill,skill_deskripsi.nama_pembuat,skill_deskripsi.logo_skill,skill_deskripsi.deskripsi from skill,skill_deskripsi where skill.nama_skill like ? and skill_deskripsi.id_skill=skill.id_skill", "%"+NamaSkill+"%")
			row := database.SelectDatabyNama(NamaSkill)
			for row.Next() {
				var err = row.Scan(
					&SkillDes.NamaSkill,
					&SkillDes.NamaPembuat,
					&SkillDes.LogoSkill,
					&SkillDes.Deskripsi)
				switch {
				case err != nil:
					fmt.Println(err.Error())
					return
				default:
					ArrSkillDes = append(ArrSkillDes, SkillDes)
					response.Status = 1
					response.Size = Skill.Total
					response.Message = " Nama Skill Ada, ID Kategori= - "
					response.Data = ArrSkillDes
				}
			}
		default:
			response.Status = 1
			response.Size = Skill.Total
			response.Message = " Nama Skill Tidak Ada, ID Kategori= - "
		}
	default:
		var Skill2 models.Skill
		// query2 := db.QueryRow("select COUNT(skill.nama_skill) from skill,skill_deskripsi where (skill.id_kategori_skill=? and skill.nama_skill like ? ) and skill_deskripsi.id_skill=skill.id_skill", IDKategori, "%"+NamaSkill+"%").Scan(&Skill2.Total)
		query2, Skill2 := database.SelectSkillbyNama2(IDKategori, NamaSkill)
		switch {
		case query2 == nil && Skill2.Total > 0:
			//row, _ := db.Query("SELECT skill.nama_skill,skill_deskripsi.nama_pembuat,skill_deskripsi.logo_skill,skill_deskripsi.deskripsi from skill,skill_deskripsi where skill.nama_skill like ? and skill_deskripsi.id_skill=skill.id_skill and id_kategori_skill=?", "%"+NamaSkill+"%", IDKategori)
			row := database.SelectDatabyNama2(NamaSkill, IDKategori)
			for row.Next() {
				var err = row.Scan(
					&SkillDes.NamaSkill,
					&SkillDes.NamaPembuat,
					&SkillDes.LogoSkill,
					&SkillDes.Deskripsi)
				switch {
				case err != nil:
					fmt.Println(err.Error())
					return
				default:
					ArrSkillDes = append(ArrSkillDes, SkillDes)
					response.Status = 1
					response.Size = Skill2.Total
					response.Message = " Nama Skill Ada, ID Kategori= " + IDKategori
					response.Data = ArrSkillDes
				}
			}
		default:
			response.Status = 1
			response.Size = Skill2.Total
			response.Message = " Maaf Tidak Ada"
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiStatusSkill(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Skill models.Skill

	db := database.Connect()
	defer db.Close()

	IDSkill := r.FormValue("id_skill")
	StatusSkill := r.FormValue("status_skill")
	// query, total = database.SelectStatusSkill(IDSkill)
	//query := db.QueryRow("select count(nama_skill) as total from  skill where id_skill=? ", IDSkill).Scan(&Skill.Total)
	query, Skill := database.SelectNamaSkill(IDSkill)
	switch {
	case query == nil && Skill.Total > 0:
		log.Println("lakukan update")
		doUpdate := database.UpdateStatusSkill(StatusSkill, IDSkill)
		switch {
		case doUpdate == nil:
			log.Println("Status_Update")
		}
		response.Status = 1
		response.Message = "Berhasil update"
	default:
		response.Status = 0
		response.Message = "id_skill tidak ditemukan"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiSkillKategori(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Total models.Skill

	db := database.Connect()
	defer db.Close()

	ID_Kategori := r.FormValue("id_kategori_skill")
	Nama_Kategori := r.FormValue("nama_kategori")
	//query := db.QueryRow("Select count(id_kategori_skill) as total from skill_kategori where id_kategori_skill=? ", ID_Kategori).Scan(&Total.Total)
	query, Total := database.SelectIDKategoriSkill(ID_Kategori)
	log.Println(Total.Total)
	switch {
	case query == nil && Total.Total > 0:
		doUpdate := database.UpdateSkillKategori(ID_Kategori, Nama_Kategori)
		switch {
		case doUpdate == nil:
			log.Println("Status_Update")
		}
		response.Status = 1
		response.Message = "Berhasil di Update"
	default:
		response.Status = 0
		response.Message = "ID Kategori Skill tidak Ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiSkillCommand(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Total models.Skill

	db := database.Connect()
	defer db.Close()

	IDSkillCMD := r.FormValue("id_skill_command")
	Pertanyaan := r.FormValue("pertanyaan")
	Jawaban := r.FormValue("jawaban")
	Topic := r.FormValue("topic")

	// query := db.QueryRow("Select count(id_skill_command) from skill_command WHERE id_skill_command =? ", IDSkillCMD).Scan(&Total.Total)
	query, Total := database.SelectSkillCommand(IDSkillCMD)
	switch {
	case query == nil && Total.Total > 0:
		log.Println("ada")
		doUpdate := database.UpdateSkillCommand(IDSkillCMD, Pertanyaan, Jawaban, Topic)
		response.Status = 1
		response.Message = "Berhasil di Update"
		switch {
		case doUpdate == nil:
			log.Println("Status_Update")
		}
	default:
		response.Status = 0
		response.Message = "id_skill_command tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiSkillDeskripsi(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseSkill
	var Total models.Skill

	db := database.Connect()
	defer db.Close()

	IDSkill := r.FormValue("id_skill")
	NamaPembuat := r.FormValue("nama_pembuat")
	Logo := r.FormValue("logo_skill")
	Deskripsi := r.FormValue("deskripsi")

	// query := db.QueryRow("Select count(id_skill) from skill_deskripsi WHERE id_skill =? ", IDSkill).Scan(&Total.Total)
	query, Total := database.SelectSkillDeskripsi(IDSkill)
	switch {
	case query == nil && Total.Total > 0:
		log.Println("ada")
		doUpdate := database.UpdateSkillDeskripsi(IDSkill, NamaPembuat, Logo, Deskripsi)
		response.Status = 1
		response.Message = "Berhasil di Update"
		switch {
		case doUpdate == nil:
			log.Println("Status_Update")
		}
	default:
		response.Status = 0
		response.Message = "id_skill tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func HapusSkillCommand(w http.ResponseWriter, r *http.Request) {
	db := database.Connect()
	defer db.Close()
	var response models.ResponseSkill
	IDSkillcmd := r.FormValue("id_skill_command")
	query := database.HapusSkillCommand(IDSkillcmd)
	cekdelete, _ := query.RowsAffected()
	if cekdelete == 0 {
		log.Println("ini row cek")
	}
	response.Status = 1
	response.Message = " Skill Command Berhasil di Hapus"
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func HapusSkill(w http.ResponseWriter, r *http.Request) {
	db := database.Connect()
	defer db.Close()
	var response models.ResponseSkill
	IDSkill := r.FormValue("id_skill")
	query := database.HapusSkilldb(IDSkill)
	cekdelete, _ := query.RowsAffected()
	if cekdelete == 0 {
		log.Println("ini row cek")
	}
	response.Status = 1
	response.Message = " Skill Berhasil di Hapus"
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
