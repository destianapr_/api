package models

type Skill struct {
	IDKategoriSkill int    `json:"id_kategori_skill"`
	NamaSkill       string `json:"nama_skill"`
	StatusSkill     int    `json:"status_skill"`
	Total           int    `json:"total"`
}

type Skill2 struct {
	IDKategoriSkill int    `json:"id_kategori_skill"`
	StatusSkill     int    `json:"status_skill"`
	AlamatSurel     string `json:"alamat_surel"`
	Total           int    `json:"total"`
}

type Skill_db struct {
	IDKategoriSkill int    `json:"id_kategori_skill"`
	NamaSkill       string `json:"nama_skill"`
	StatusSkill     int    `json:"status_skill"`
}

type SkillDes_db struct {
	IDSkill     int    `json:"id_skill"`
	NamaPembuat string `json:"nama_pembuat"`
	NamaSkill   string `json:"nama_skill"`
	LogoSkill   string `json:"logo_skill"`
	Deskripsi   string `json:"deskripsi"`
	Auth        string `json:"auth"`
	Discover    string `json:"discover"`
}

type Kategori struct {
	IDKategori   string `json:"id_kategori_skill"`
	NamaKategori string `json:"nama_kategori"`
}

type CommandSkill struct {
	Pertanyaan string `json:"pertanyaan"`
	Jawaban    string `json:"jawaban"`
	Topic      string `json:"topic"`
}

type ReviewSkill struct {
	Review        string `json:"review"`
	Rating        int    `json:"rating"`
	EmailReviewer string `json:"email_reviewer"`
}

type SkillAuth struct {
	IP        string `json:"ip"`
	Port      string `json:"port"`
	Unique_id string `json:"unique_id"`
	Token     string `json:"token"`
}

type STotal struct {
	Total int `json:"total"`
}

type ResponseSkill struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ResponseSizeSkill struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Size    int         `json:"size"`
	Data    interface{} `json:"data"`
}
