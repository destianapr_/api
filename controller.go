package controller

import (
	"SmartSpeaker/Api/Ability/models"
	"SmartSpeaker/Api/database"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func TambahAbility(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseAbility
	//cobatestingvar AAbility models.Ability
	var AAbility_db models.Ability_db
	db := database.Connect()
	defer db.Close()
	Nama := r.FormValue("nama")
	IMG_Path := r.FormValue("img_path")
	IdKategori := r.FormValue("id_kategori")
	//query := db.QueryRow("SELECT count(nama)as total from ability where nama=? and img_path=?",Nama,IMG_Path).Scan(&AAbility.Total)
	// query := db.QueryRow("SELECT count(nama)as total, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(id_kategori, 0)) as id_kategori, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability where nama=? and img_path=? and id_kategori=?", Nama, IMG_Path, IdKategori).
	// 	Scan(
	// 		&AAbility.Total,
	// 		&AAbility_db.IDAbility,
	// 		&AAbility_db.IDKategori,
	// 		&AAbility_db.NamaAbility,
	// 		&AAbility_db.ImgPath)
	query, AAbility, AAbility_db := database.SelectAbility(Nama, IMG_Path, IdKategori)
	switch {
	case (query == nil) && (AAbility.Total > 0):
		response.Status = 0
		response.Message = "Ability Sudah Ada"
		response.Data = AAbility_db
	case AAbility.Total == 0:
		result := database.InsertAbility(Nama, IMG_Path, IdKategori)
		if result == nil {
			log.Println("Ability Berhasil di tambahkan")
		}
		response.Status = 1
		response.Message = "Ability Berhasil di Tambahkan"
		response.Data = AAbility_db
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func TambahAbilityCMD(w http.ResponseWriter, r *http.Request) {
	var response models.ResponseAbilityCMD
	db := database.Connect()
	defer db.Close()
	Command := r.FormValue("command")
	id_Ability := r.FormValue("id_ability")
	result := database.InsertAbilityCMD(id_Ability, Command)
	if result == nil {
		log.Println("Ability_cmd Berhasil di tambahkan")
	}
	response.Status = 1
	response.Message = "Command Ability Berhasil di Tambahkan"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAbility(w http.ResponseWriter, r *http.Request) {
	var response models.AmbilAbility
	var AAbility models.Ability
	var AAbility_db models.Ability_db
	var ArrAAbility_db []models.Ability_db
	db, _ := database.Connect2()
	defer db.Close()

	// row, _ := db.Query("SELECT count(id_ability) as total,(COALESCE(id_kategori, 0)) as Kategori, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability group by id_ability ")
	row := database.SelectDataAbility()
	for row.Next() {
		var err2 = row.Scan(
			&AAbility.Total,
			&AAbility_db.IDAbility,
			&AAbility_db.IDKategori,
			&AAbility_db.NamaAbility,
			&AAbility_db.ImgPath)
		if err2 != nil {
			fmt.Println("cek")
			return
		} else {
			ArrAAbility_db = append(ArrAAbility_db, AAbility_db)
			if len(ArrAAbility_db) < 1 {
				log.Println("0")
			} else {
				response.Status = 1
				response.Message = "Ability Ada"
				response.Size = AAbility.Total
				response.Data = ArrAAbility_db
			}
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAbilityCMD(w http.ResponseWriter, r *http.Request) {
	var response models.AmbilAbilityCMD
	var AAbility models.Ability
	var AAbility_db models.AbilityCMD_db
	var ArrAAbility_db []models.AbilityCMD_db
	db, err := database.Connect2()
	defer db.Close()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	IDAbility := r.FormValue("id_ability")
	// query := db.QueryRow("SELECT count(id_ability)as total from ability_cmd where id_ability=?", IDAbility).Scan(&AAbility.Total)
	query, AAbility := database.SelectAbilityCommand(IDAbility)
	if query == nil && AAbility.Total > 0 {
		//row, err := db.Query("SELECT count(id_ability)as total, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(command, '-')) as command from ability_cmd where id_ability=? group by id_ability_cmd ", IDAbility)
		row := database.SelectDataAbilityCommand(IDAbility)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		for row.Next() {
			var err = row.Scan(
				&AAbility.Total,
				&AAbility_db.IDAbility,
				&AAbility_db.Command)
			if err != nil {
				fmt.Println(err.Error())
				return
			} else {
				ArrAAbility_db = append(ArrAAbility_db, AAbility_db)
				if len(ArrAAbility_db) > 1 {
					log.Println("0")
				} else {
					response.Status = 1
					response.Message = "Ability_cmd Ada"
					response.Data = ArrAAbility_db
				}
			}
		}
	} else {
		response.Status = 0
		response.Message = "Ability_cmd tidak ada Ada"
		response.Data = ArrAAbility_db
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AmbilAbilityKategori(w http.ResponseWriter, r *http.Request) {
	var response models.AmbilAbility
	var AAbility models.Ability
	var AAbility_db models.Ability_db
	var ArrAAbility_db []models.Ability_db
	db, _ := database.Connect2()
	defer db.Close()

	Kategori := r.FormValue("id_kategori")
	//row, _ := db.Query("SELECT count(id_kategori) as total, (COALESCE(id_ability, 0)) as id_ability,(COALESCE(id_kategori, 0)) as Kategori, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability where id_kategori=? group by id_ability ", Kategori)
	row := database.SelectDataAbilityKategori(Kategori)
	defer row.Close()
	for row.Next() {
		var err2 = row.Scan(
			&AAbility.Total,
			&AAbility_db.IDAbility,
			&AAbility_db.IDKategori,
			&AAbility_db.NamaAbility,
			&AAbility_db.ImgPath)
		if err2 != nil {
			fmt.Println("Data")
			return
		} else {
			ArrAAbility_db = append(ArrAAbility_db, AAbility_db)
			if len(ArrAAbility_db) < 1 {
				log.Println("0")
			} else {
				response.Status = 1
				response.Message = "Ability Ada"
				response.Size = AAbility.Total
				response.Data = ArrAAbility_db
			}
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiAbility(w http.ResponseWriter, r *http.Request) {
	var response models.AmbilAbility
	var AAbility models.Ability

	db := database.Connect()
	defer db.Close()

	IDAbility := r.FormValue("id_ability")
	Nama := r.FormValue("nama")
	IMGPath := r.FormValue("img_path")
	//cek := db.QueryRow("Select count(id_ability) as total from ability where id_ability=?", IDAbility).Scan(&AAbility.Total)
	cek, AAbility := database.SelectIDAbility(IDAbility)
	if cek == nil && AAbility.Total > 0 {
		log.Println("lakukan update")
		//query, _ := db.Exec("UPDATE ability set nama=?, img_path=? where id_ability=?", Nama, IMGPath, IDAbility)
		query := database.UpdateAbility(Nama, IMGPath, IDAbility)
		if query != nil {
			log.Println("Ability update")
		}
		response.Status = 1
		response.Message = "Nama dan IMGPath berhasil di update"
	} else {
		response.Status = 0
		response.Message = "IDAbility tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func GantiAbilityCMD(w http.ResponseWriter, r *http.Request) {
	var response models.AmbilAbility
	var AAbility models.Ability
	db := database.Connect()
	defer db.Close()
	IDAbilityCMD := r.FormValue("id_ability_cmd")
	Command := r.FormValue("command")
	//cek := db.QueryRow("Select count(id_ability_cmd) as total from ability_cmd where id_ability_cmd=?", IDAbilityCMD).Scan(&AAbility.Total)
	cek, AAbility := database.SelectAbilityCommand(IDAbilityCMD)
	if cek == nil && AAbility.Total > 0 {
		log.Println("lakukan update")
		//query, _ := db.Exec("UPDATE ability_cmd set command=? where id_ability_cmd=?",Command,IDAbilityCMD)
		query := database.UpdateAbilityCMD(Command, IDAbilityCMD)
		if query != nil {
			log.Println("Ability update")
		}
		response.Status = 1
		response.Message = "Command berhasil di update"
	} else {
		response.Status = 0
		response.Message = "IDAbilityCMD tidak ada"
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func HapusAbilityCMD(w http.ResponseWriter, r *http.Request) {
	db := database.Connect()
	defer db.Close()
	var response models.ResponseAbilityCMD
	var AAbility models.Ability
	IDAbilityCMD := r.FormValue("id_ability_cmd")
	cek := db.QueryRow("Select count(id_ability_cmd) as total from ability_cmd where id_ability_cmd=?", IDAbilityCMD).Scan(&AAbility.Total)
	log.Println(AAbility.Total)
	if cek == nil && AAbility.Total > 0 {
		//query,_ := db.Exec("Delete from ability_cmd where id_ability_cmd=?",IDAbilityCMD)
		query := database.HapusAbilityCMD(IDAbilityCMD)
		cekdelete, _ := query.RowsAffected()
		if cekdelete == 0 {
			log.Println("ini row cek")
		}
		response.Status = 1
		response.Message = " Ability Command Berhasil di Hapus"
	} else {
		response.Status = 0
		response.Message = "ID Ability tidak Ada"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
