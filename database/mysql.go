package database

import (
	Ability "SmartSpeaker/Api/Ability/models"
	Skill "SmartSpeaker/Api/Skill/models"
	"SmartSpeaker/Api/UserSkill/models"
	UsersModel "SmartSpeaker/Api/otentifikasi_pengguna/models"
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/smartspeaker")

	if err != nil {
		log.Fatal(err)
	}
	return db
}

func Connect2() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/smartspeaker")
	if err != nil {
		return nil, err
	}
	return db, nil
}

func InsertTambahSS(machine_id string, email string, nama_device string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("INSERT INTO SS_device (id_ss_device, email, nama_device) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE email= VALUES(email);", machine_id, email, nama_device)

	if err != nil {
		log.Println("query error")
	}

	return result
}

func SelectTambahSS(email string, machine_id string) (error, int) {
	var hitung int

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as count_email FROM user_device WHERE email=? and id_device=?", email, machine_id).Scan(&hitung)
	log.Printf("hitung: %d", hitung)

	return query, hitung
}

func UpdateTambahSS(device_name string, category int, id_skill int, email string, machine_id string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("UPDATE user_device SET nama_device=?, id_kategori_device=?, id_skill=? WHERE email=? and id_device=?", device_name, category, id_skill, email, machine_id)

	if err != nil {
		log.Fatalf("query error: %v", err)
	}

	return result
}

func InsertUserDevice(device_name string, category int, id_skill int, machine_id string, email string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("INSERT into user_device(nama_device, id_kategori_device, id_skill, id_device, email) VALUES(?, ?, ?, ?, ?)", device_name, category, id_skill, machine_id, email)

	if err != nil {
		log.Println("query error")
	}
	return result
}

func SelectTambahOtentifikasi(email string, unique string) (error, int) {
	var hitung int
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as count_email FROM skill_auth_external WHERE email=? and unique_id=?", email, unique).Scan(&hitung)
	return query, hitung
}

func SelectDeviceByEmail(ValueAlamatSurel string, ValueIDSkill string) (error, int) {
	var hitung int
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(email) as jumlah from user_device where email=? and id_skill=? ", ValueAlamatSurel, ValueIDSkill).Scan(&hitung)
	return query, hitung
}

func SelectUserDevice(email string, unique string) (error, int) {
	var hitung int
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as count_email FROM user_device WHERE email=? and id_device=?", email, unique).Scan(&hitung)
	return query, hitung
}

func UpdateUserDevice(device_name string, category int, id_skill int, email string, unique string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("UPDATE user_device SET nama_device=?, id_kategori_device=?, id_skill=? WHERE email=? and id_device=?", device_name, category, id_skill, email, unique)

	if err != nil {
		log.Println("query error")
	}

	return result
}

func SelectTambahPerangkat(email string, unique string) (error, int) {
	var hitung int

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as count_email FROM skill_auth_external WHERE email=? and unique_id=?", email, unique).Scan(&hitung)

	return query, hitung
}

func UpdateTambahPerangkat(token string, email string, unique string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("UPDATE skill_auth_external SET token=? WHERE email=? and unique_id=?", token, email, unique)

	if err != nil {
		log.Println("query error")
	}

	return result
}

func InsertTambahPerangkat(id_skill int, email string, token string, ip_address string, port int, unique string) sql.Result {
	db := Connect()
	defer db.Close()

	result, err := db.Exec("INSERT into skill_auth_external(id_skill, email, token, ip, port, unique_id) VALUES(?, ?, ?, ?, ?, ?)", id_skill, email, token, ip_address, port, unique)

	if err != nil {
		log.Println("query error")
	}

	return result
}

func SelectTambahPerangkat2(email string, unique string) (error, int) {
	var id int

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT id_skill_auth_external FROM skill_auth_external where email=? and unique_id=?", email, unique).Scan(&id)

	return query, id
}

func SelectDaftar(email string, username string) (error, UsersModel.Users, UsersModel.Users) {
	var users UsersModel.Users
	var users2 UsersModel.Users

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(if(email=?, 1, null)) as jmlh_email, count(if(username=?, 1, null)) as jmlh_username FROM user", email, username).Scan(&users.Jumlah, &users2.Jumlah)

	return query, users, users2
}

func InsertDaftar(email string, username string, pass []byte, token string) error {
	db := Connect()
	defer db.Close()

	_, query := db.Exec("INSERT INTO user (email, username, password, uid) values (?,?,?,?)", email, username, pass, token)

	return query
}

func SelectVerifUlang(email string) (error, UsersModel.Users) {
	var users UsersModel.Users

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT COUNT(email) as jmlh, uid, status FROM user WHERE email=?", email).Scan(&users.Jumlah, &users.UID, &users.Status)

	return query, users
}

func UpdateVerif(uid string) error {
	db := Connect()
	defer db.Close()

	_, query := db.Exec("UPDATE user SET status = 1 WHERE uid = ?", uid)

	return query
}
func SelectLogin(username string, email string) (error, UsersModel.Users, UsersModel.Tag) {
	var users UsersModel.Users
	var tag UsersModel.Tag

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as jumlah , (COALESCE(otoritas, 0)) as otoritas, (COALESCE(status, 0)) as status, (COALESCE(uid, 0)) as uid, (COALESCE(email, '-')) as email, (COALESCE(username, '-')) as username, (COALESCE(password, '-')) as password FROM user WHERE (username=? or email=?)", username, email).Scan(&users.Jumlah, &tag.Otoritas, &users.Status, &tag.Uid, &tag.Email, &tag.Username, &users.KataSandi)

	return query, users, tag
}

func SelectLupaSandi(email string) (error, UsersModel.Users) {
	var users UsersModel.Users

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT COUNT(email) as jmlh FROM user WHERE email=?", email).Scan(&users.Jumlah)

	return query, users
}

func UpdateLupaSandi(pass []byte, email string) error {
	db := Connect()
	defer db.Close()

	_, query := db.Exec("UPDATE user SET password = ? WHERE email = ?", pass, email)

	return query
}

func SelectAturSandi(email string, username string) (error, UsersModel.Users) {
	var users UsersModel.Users

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT COUNT(email or username) as jmlh, password FROM user WHERE email=? or username=?", email, username).Scan(&users.Jumlah, &users.KataSandi)

	return query, users
}

func UpdateAturSandi(pass []byte, email string, username string) error {
	db := Connect()
	defer db.Close()

	_, query := db.Exec("UPDATE user SET password = ? WHERE email = ? or username = ?", pass, email, username)

	return query
}

// func Selectability(nama string, img_path string, id_kategori string) (error, Ability.Ability, Ability.Ability, Ability.Ability) {
// 	var totalnama Ability.Ability
// 	var totalimgpath Ability.Ability
// 	var totalkategori Ability.Ability
// 	db := Connect()
// 	defer db.Close()
// 	//query := db.QueryRow("SELECT count(if(nama=?, 1, null)) as totalnama, count(if(img_path=?, 1, null)) as totalimgpath FROM ability", nama, img_path).
// 	//	Scan(&totalnama.Total, &totalimgpath.Total)
// 	//return query, totalnama, totalimgpath
// 	query := db.QueryRow("SELECT count(if(nama=?, 1, null)) as totalnama, count(if(img_path=?, 1, null)) as totalimgpath, count(if(id_kategori=?, 1, null)) as totalkategori FROM ability", nama, img_path, id_kategori).
// 		Scan(&totalnama.Total, &totalimgpath.Total, &totalkategori.Total)
// 	return query, totalnama, totalimgpath, totalkategori
// }

func SelectNama_skill(Nama_skill string) (error, models.InfoSkill, models.InfoSkill_db) {
	var Infoskill models.InfoSkill
	var Infoskill_db models.InfoSkill_db
	db := Connect()
	defer db.Close()
	query := db.QueryRow("Select COUNT(skill.nama_skill) as total, (COALESCE(skill_deskripsi.nama_pembuat, '-')) as nama_pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) as logo_skill, (COALESCE(skill_deskripsi.deskripsi, '-')) as deskripsi, (COALESCE(skill.nama_skill, '-')) as nama_skill, (COALESCE(user_skill.id_skill, 0 )) as id_skill from skill, skill_deskripsi, user_skill where skill.nama_skill LIKE ? and user_skill.id_skill=skill.id_skill and skill.id_skill=skill_deskripsi.id_skill ", "%"+Nama_skill+"%").
		Scan(
			&Infoskill.Total,
			&Infoskill_db.NamaPembuat,
			&Infoskill_db.LogoSkill,
			&Infoskill_db.Deskripsi,
			&Infoskill_db.NamaSkill,
			&Infoskill_db.IDSkill)
	return query, Infoskill, Infoskill_db
}

func SelectCek(Alamat_surel string, Id_Skill string) (error, models.Cek) {
	var cek models.Cek
	db := Connect()
	defer db.Close()
	query := db.QueryRow("SELECT COUNT(email) from user_skill where id_skill= ? and email = ?", Id_Skill, Alamat_surel).
		Scan(&cek.Count)
	//if query != nil {
	//	log.Println("query error")
	//}
	return query, cek
}

func Hapus(Alamat_surel string, Id_Skill string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("delete from user_skill where email =? and id_skill=?", Alamat_surel, Id_Skill)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func SelectSkill(Alamat_surel string) (error, models.UserSkill, models.UserSkill_db) {
	var USkill models.UserSkill
	var USkill_db models.UserSkill_db
	db := Connect()
	defer db.Close()
	query := db.QueryRow("SELECT COUNT(email) as total, (COALESCE(email, '-')) as alamat_surel, (COALESCE(id_skill, 0)) as id_skill from  user_skill where email= ?", Alamat_surel).
		Scan(&USkill.Total,
			&USkill_db.AlamatSurel,
			&USkill_db.IDSkill)
	return query, USkill, USkill_db
}

func InsertSkillBawaan(Alamat_Surel string, Id_Skill int) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("INSERT INTO user_skill(email, id_skill) VALUES(?,?)", Alamat_Surel, Id_Skill)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func InsertSkillBaru(Alamat_surel string, Id_Skill string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("INSERT INTO user_skill(email, id_skill) Values(?,?)", Alamat_surel, Id_Skill)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func SelectAmbilInfoSkill(Alamat_surel string) (error, models.InfoSkill, models.InfoSkill_db) {
	var InfoSkill models.InfoSkill
	var Infoskill_db models.InfoSkill_db
	db := Connect()
	defer db.Close()
	//query := db.QueryRow("SELECT COUNT(email) as Total (COALESCE(skill_deskripsi.nama_pembuat, '-')) AS Nama_Pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) AS Logo_Skill, (COALESCE(skill_deskripsi.deskripsi, '-')) AS Deskripsi, (COALESCE(skill.nama_skill, '-')) AS Nama_Skill FROM user_skill, skill, skill_deskripsi WHERE email= ? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill ", Alamat_surel).
	//query := db.QueryRow("SELECT (COALESCE(nama_pembuat, '-')) AS Nama_Pembuat, (COALESCE(logo_skill, '-')) AS Logo_Skill, (COALESCE(deskripsi, '-')) AS Deskripsi, (COALESCE(nama_skill, '-')) AS Nama_Skill FROM user_skill, skill, skill_deskripsi WHERE user_skill= ? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill ", Alamat_surel).
	query := db.QueryRow("SELECT COUNT(email) as Total,  (COALESCE(skill.id_skill, '-')) AS id_skill, (COALESCE(skill_deskripsi.nama_pembuat, '-')) AS Nama_Pembuat, (COALESCE(skill_deskripsi.logo_skill, '-')) AS Logo_Skill, (COALESCE(skill_deskripsi.deskripsi, '-')) AS Deskripsi, (COALESCE(skill.nama_skill, '-')) AS Nama_Skill FROM user_skill, skill, skill_deskripsi WHERE user_skill.email= ? AND user_skill.id_skill=skill.id_skill AND skill.id_skill=skill_deskripsi.id_skill ", Alamat_surel).
		Scan(&InfoSkill.Total,
			&Infoskill_db.IDSkill,
			&Infoskill_db.NamaPembuat,
			&Infoskill_db.LogoSkill,
			&Infoskill_db.Deskripsi,
			&Infoskill_db.NamaSkill)
	return query, InfoSkill, Infoskill_db
}

func HapusAbilityCMD(IdAbilityCMD string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("Delete from ability_cmd where id_ability_cmd=?", IdAbilityCMD)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func UpdateAbilityCMD(Command string, IDAbilityCMD string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("UPDATE ability_cmd set command=? where id_ability_cmd=?", Command, IDAbilityCMD)
	if err != nil {
		log.Println("query error")
	}
	return result
}

// func SelectSIDSkill(IDSkill string) (error, Skill.Skill) {
// 	var cek Skill.Skill
// 	db := Connect()
// 	defer db.Close()

// 	query := db.QueryRow("select count(nama_skill) as total from  skill where id_skill=? ", IDSkill).Scan(&cek.Total)
// 	return query, cek
// }

//===SKILL==================================================================================================================
//**************************************************************************************************************************

func SelectSkillbyNamadanIDaKategori(NamaSkill string, IDKategori string) (error, Skill.Skill, Skill.Skill_db) {
	var Skill1 Skill.Skill
	var Skill_db Skill.Skill_db
	db := Connect()
	defer db.Close()
	query := db.QueryRow("Select Count(nama_skill) as total, (coalesce(id_kategori_skill, 0)) as iDKategory, (coalesce(nama_skill, '-')) as nama_skill, (coalesce(status_skill, 0)) as status_skill from skill where nama_skill=? && id_kategori_skill=?", NamaSkill, IDKategori).
		Scan(&Skill1.Total,
			&Skill_db.IDKategoriSkill,
			&Skill_db.NamaSkill,
			&Skill_db.StatusSkill,
		)
	return query, Skill1, Skill_db
}

func UpdateStatusSkill(StatusSkill string, IDSkill string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("update skill set status_skill=? where id_skill=?", StatusSkill, IDSkill)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func HapusSkillCommand(IDSkillcmd string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec(" Delete from skill_command where id_skill_command=?", IDSkillcmd)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func HapusSkilldb(Id_Skill string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("delete from skill where id_skill=?", Id_Skill)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func UpdateSkillKategori(IDKategori string, Namakategori string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("update skill_kategori set nama_kategori=? where id_kategori_skill=?", Namakategori, IDKategori)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func UpdateSkillCommand(ID string, P string, J string, T string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("update skill_command set pertanyaan=?, jawaban=?, topic=? where id_skill_command =?", P, J, T, ID)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func UpdateSkillDeskripsi(ID string, N string, L string, D string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("update skill_deskripsi set nama_pembuat=?, logo_skill=?, deskripsi=? where id_skill=?", N, L, D, ID)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func InsertTambahSkill(NamaSkill string, IDKategori string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("Insert into skill(nama_skill,id_kategori_skill) values (?,?)", NamaSkill, IDKategori)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func InsertSkillCommand(ID_skill string, Pertanyaaan string, Jawaban string, Topic string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("Insert into skill_command(id_skill, pertanyaan, jawaban, topic) values (?,?,?,?)", ID_skill, Pertanyaaan, Jawaban, Topic)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func InsertSkillDeskripsi(id_skill string, nama_pembuat string, logo_skill string, deskripsi string, auth string, discover string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("Insert into skill_deskripsi(id_skill,nama_pembuat,logo_skill,deskripsi,auth,discover) values(?,?,?,?,?,?)", id_skill, nama_pembuat, logo_skill, deskripsi, auth, discover)

	if err != nil {
		log.Println(err)
	}
	return result
}

func SelectSkillKategori(NamaKategori string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(nama_kategori) as total from skill_kategori where nama_kategori=? ", NamaKategori).Scan(&Skill1.Total)
	return query, Skill1

}

func InsertSkillKategori(NamaKategori string) sql.Result {
	db := Connect()
	defer db.Close()
	row, err := db.Exec("Insert Into skill_kategori(nama_kategori) values(?)", NamaKategori)

	if err != nil {
		log.Println(err)
	}
	return row
}

func InsertSkillReview(ID_skill string, Review string, Rating string, EmailReviewer string) sql.Result {
	db := Connect()
	defer db.Close()
	row, err := db.Exec("Insert into skill_review(id_skill, review, rating, email_reviewer) values (?,?,?,?)", ID_skill, Review, Rating, EmailReviewer)

	if err != nil {
		log.Println(err)
	}
	return row
}
func SelectStatusSkill(StatusSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(id_skill) as total from skill where status_skill=?", StatusSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDatabyStatusSkill(StatusSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()

	result, _ := db.Query("SELECT nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=? and skill.id_skill=skill_deskripsi.id_skill", StatusSkill)
	return result
}

func SelectIDKategori(StatusSkill string, IDKategori string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(id_skill) as total from skill where status_skill=? and id_kategori_skill=?", StatusSkill, IDKategori).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDatabyKategori(StatusSkill string, IDKategori string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT nama_pembuat, logo_skill, deskripsi, nama_skill from skill,skill_deskripsi where status_skill=? and id_kategori_skill=? and skill.id_skill=skill_deskripsi.id_skill", StatusSkill, IDKategori)
	return result
}

func SelectAuthByEmail(Alamat_surel string, IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(email) as total from skill_auth_external where email=? and id_skill=?", Alamat_surel, IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataAuthByEmail(Alamat_surel string, IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT ip, port, unique_id,token from skill_auth_external where emaiL=? and id_skill=? ", Alamat_surel, IDSkill)
	return result
}

func SelectAuthBySS(IDSSDevice string) (error, Skill.Skill2) {
	var Skill1 Skill.Skill2
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(email) as total, email from ss_device where id_ss_device=?", IDSSDevice).Scan(&Skill1.Total, &Skill1.AlamatSurel)
	return query, Skill1
}

func SelectCekAuthBySS(AlamatSurel string, IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill

	db := Connect()
	defer db.Close()

	cek := db.QueryRow("Select count(email) as total from  skill_auth_external where email = ? and id_skill=?", AlamatSurel, IDSkill).Scan(&Skill1.Total)
	return cek, Skill1
}

func SelectDataAuthBySS(Alamat_surel string, IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("select ip,port,unique_id,token from skill_auth_external where email=? and id_skill=?", Alamat_surel, IDSkill)
	return result
}

func SelectAuthSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select count(id_skill) as total from  skill_deskripsi where id_skill=?", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataAuthSkill(IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("Select auth from skill_deskripsi where id_skill=?", IDSkill)
	return result
}

func SelectDiscoverSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select count(id_skill) as total from  skill_deskripsi where id_skill=? ", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataDiscoverSkill(IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("Select discover from skill_deskripsi where id_skill=?", IDSkill)
	return result
}

func SelectKategoriSkill() (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_kategori_skill) as total from skill_kategori").Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataKategoriSkill() *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("Select count(id_kategori_skill) as total, id_kategori_skill, nama_kategori from skill_kategori group by id_kategori_skill")
	return result
}

func SelectReviewSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select count(review) as total from skill_review where id_skill=?", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataReviewSkill(IDSkill string, Size2 int) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("select count(review) as total, review,rating,email_reviewer from skill_review where id_skill=? group by id_skill_review limit ?", IDSkill, Size2)
	return result
}

func SelectRatingSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_skill) as total from skill_review where id_skill=?", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataRatingSkill(IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT (sum(rating) div count(rating)) as total from skill_review where id_skill=?", IDSkill)
	return result
}

func SelectCommandSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_skill) as total from skill_command where id_skill=?", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDataCommandSkill(IDSkill string, Size2 int) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT pertanyaan,jawaban,topic from skill_command where id_skill=? limit ?", IDSkill, Size2)
	return result
}

func SelectTopic(Topic string, IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_skill) as total from skill_command where topic=? and id_skill=?", Topic, IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDatabyTopicSkill(Topic string, IDSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT pertanyaan,jawaban,topic from skill_command where topic=? && id_skill=?", Topic, IDSkill)
	return result
}

func SelectIDKategoriSkill(ID_Kategori string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_kategori_skill) as total from skill_kategori where id_kategori_skill=? ", ID_Kategori).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectSkillDeskripsi(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_skill) from skill_deskripsi WHERE id_skill =? ", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectSkillCommand(IDSkillCMD string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select count(id_skill_command) from skill_command WHERE id_skill_command =? ", IDSkillCMD).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectNamaSkill(IDSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select count(nama_skill) as total from  skill where id_skill=? ", IDSkill).Scan(&Skill1.Total)
	return query, Skill1
}

func SelectSkillbyNama1(NamaSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("Select Count(skill.nama_skill)as total from skill, skill_deskripsi where skill_deskripsi.id_skill=skill.id_skill and skill.nama_skill like ?", "%"+NamaSkill+"%").Scan(&Skill1.Total)
	return query, Skill1
}

func SelectSkillbyNama2(IDKategori string, NamaSkill string) (error, Skill.Skill) {
	var Skill1 Skill.Skill
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select COUNT(skill.nama_skill) from skill,skill_deskripsi where (skill.id_kategori_skill=? and skill.nama_skill like ? ) and skill_deskripsi.id_skill=skill.id_skill", IDKategori, "%"+NamaSkill+"%").Scan(&Skill1.Total)
	return query, Skill1
}

func SelectDatabyNama(NamaSkill string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT skill.nama_skill,skill_deskripsi.nama_pembuat,skill_deskripsi.logo_skill,skill_deskripsi.deskripsi from skill,skill_deskripsi where skill.nama_skill like ? and skill_deskripsi.id_skill=skill.id_skill", "%"+NamaSkill+"%")
	return result
}

func SelectDatabyNama2(NamaSkill string, IDKategori string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT skill.nama_skill,skill_deskripsi.nama_pembuat,skill_deskripsi.logo_skill,skill_deskripsi.deskripsi from skill,skill_deskripsi where skill.nama_skill like ? and skill_deskripsi.id_skill=skill.id_skill and id_kategori_skill=?", "%"+NamaSkill+"%", IDKategori)
	return result
}

// =========================================================================================================================================
// ***Ability*******************************************************************************************************************************

func SelectAbility(Nama string, IMG_Path string, IdKategori string) (error, Ability.Ability, Ability.Ability_db) {
	var AAbility Ability.Ability
	var AAbility_db Ability.Ability_db

	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(nama)as total, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(id_kategori, 0)) as id_kategori, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability where nama=? and img_path=? and id_kategori=?", Nama, IMG_Path, IdKategori).
		Scan(
			&AAbility.Total,
			&AAbility_db.IDAbility,
			&AAbility_db.IDKategori,
			&AAbility_db.NamaAbility,
			&AAbility_db.ImgPath)
	return query, AAbility, AAbility_db
}

func InsertAbility(Nama string, IMG_Path string, IDKategori string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("INSERT INTO ability(nama, img_path, id_kategori) VALUES(?,?,?)", Nama, IMG_Path, IDKategori)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func InsertAbilityCMD(IdAbility string, Command string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("INSERT INTO ability_cmd(id_ability, command) VALUES(?,?)", IdAbility, Command)
	if err != nil {
		log.Println("query error")
	}
	return result
}

func SelectDataAbility() *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT count(id_ability) as total,(COALESCE(id_kategori, 0)) as Kategori, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability group by id_ability ")
	return result
}

func SelectAbilityCommand(IDAbility string) (error, Ability.Ability) {
	var AAbility Ability.Ability
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(id_ability)as total from ability_cmd where id_ability=?", IDAbility).Scan(&AAbility.Total)
	return query, AAbility
}

func SelectDataAbilityCommand(IDAbility string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT count(id_ability)as total, (COALESCE(id_ability, 0)) as id_ability, (COALESCE(command, '-')) as command from ability_cmd where id_ability=? group by id_ability_cmd ", IDAbility)
	return result
}

func SelectDataAbilityKategori(Kategori string) *sql.Rows {
	db, _ := Connect2()
	defer db.Close()
	result, _ := db.Query("SELECT count(id_kategori) as total, (COALESCE(id_ability, 0)) as id_ability,(COALESCE(id_kategori, 0)) as Kategori, (COALESCE(nama, '-')) as nama, (COALESCE(img_path, '-')) as img_path from ability where id_kategori=? group by id_ability ", Kategori)
	return result
}

func SelectIDAbility(IDAbility string) (error, Ability.Ability) {
	var AAbility Ability.Ability
	db := Connect()
	defer db.Close()

	query := db.QueryRow("SELECT count(id_ability)as total from ability where id_ability=?", IDAbility).Scan(&AAbility.Total)
	return query, AAbility
}

func UpdateAbility(Nama string, IMGPath string, IDAbility string) sql.Result {
	db := Connect()
	defer db.Close()
	result, err := db.Exec("UPDATE ability set nama=?, img_path=? where id_ability=?", Nama, IMGPath, IDAbility)
	if err != nil {
		log.Println("query error")
	}
	return result
}

// func SelectAuthDevice(IDSkill string,Ip string, Port string, Unique string, AlamatSurel string) (error, int) {
// 	var hitung int
// 	db := Connect()
// 	defer db.Close()

// 	query := db.QueryRow("select count(email) as total from skill_auth_external where id_skill=$id_skill and email=$email
// 	", ValueAlamatSurel, ValueIDSkill).Scan(&hitung)
// 	return query, hitung
// }

func SelectCekAuth(AlamatSurel string, IDSkill string) (error, int) {
	var cek int
	db := Connect()
	defer db.Close()

	query := db.QueryRow("select count(email) as total from skill_auth_external where id_skill=? and email=?", IDSkill, AlamatSurel).Scan(&cek)
	return query, cek
}

func InserAuthDevice(IDSkill string, AlamatSurel string, Ip string, Port string, Unique string, Token string) sql.Result {
	db := Connect()
	defer db.Close()

	result, _ := db.Exec("insert into skill_auth_external (id_skill,ip,port,unique_id,email,token) values (?,?,?,?,?,?)", IDSkill, Ip, Port, Unique, AlamatSurel, Token)
	return result
}

func UpdateAuthDevice(IDSkill string, AlamatSurel string, Ip string, Port string, Unique string, Token string) sql.Result {
	db := Connect()
	defer db.Close()

	result, _ := db.Exec("update into skill_auth_external set ip=?, port=?, unique_id=?, token=? where id_skill=? and email=?", Ip, Port, Unique, Token, IDSkill, AlamatSurel)
	return result
}
