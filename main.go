package main

import (
	ability "SmartSpeaker/Api/Ability/controller"
	Skill "SmartSpeaker/Api/Skill/controller"
	userskill "SmartSpeaker/Api/UserSkill/controller"
	otentifikasi "SmartSpeaker/Api/otentifikasi_pengguna/controller"
	tambah "SmartSpeaker/Api/tambah_perangkat_revisi2/controller"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)

	//Otentifikasi Pengguna - user/
	router.HandleFunc("/User/daftar", otentifikasi.Daftar).Methods("POST")                   // daftar / register
	router.HandleFunc("/User/verifikasiulang", otentifikasi.VerifikasiUlang).Methods("POST") // verifikasi ulang
	router.HandleFunc("/User/login", otentifikasi.Login).Methods("POST")                     // masuk / login
	router.HandleFunc("/User/lupasandi", otentifikasi.LupaSandi).Methods("POST")             // lupa sandi / forget password
	router.HandleFunc("/User/atursandi", otentifikasi.AturSandi).Methods("POST")             // atur ulang sandi / reset password
	router.HandleFunc("/User/{uid}", otentifikasi.Verifikasi).Methods("GET")                 // verifikasi

	//Tambah Perangkat - device/
	router.HandleFunc("/device/tambahss", tambah.TambahSmartSpeaker).Methods("POST")
	router.HandleFunc("/device/tambahotentifikasi", tambah.TambahOtentifikasi).Methods("POST")
	router.HandleFunc("/device/tambahperangkat", tambah.TambahPerangkat).Methods("POST")
	router.HandleFunc("/device/tambahuserdevice", tambah.TambahUserDevice).Methods("POST")
	router.HandleFunc("/device/ambildevicesurel", tambah.GetDeviceByEmail).Methods("GET")
	//router.HandleFunc("/device/", Skill.AmbilAuthBySS).Methods("POST")
	//router.HandleFunc("/device/", Skill.AmbilAuthBySS).Methods("POST")
	//router.HandleFunc("/device/", Skill.AmbilAuthBySS).Methods("POST")
	//router.HandleFunc("/device/", Skill.AmbilAuthBySS).Methods("POST")
	//router.HandleFunc("/device/", Skill.AmbilAuthBySS).Methods("POST")

	//Otentifikasi UserSkill
	router.HandleFunc("/UserSkill/TambahSkillBawaaan", userskill.TambahSkillBawaan).Methods("POST")
	router.HandleFunc("/UserSkill/Ambil", userskill.Ambil2).Methods("GET")
	router.HandleFunc("/UserSkill/Cek", userskill.Cek).Methods("GET")
	router.HandleFunc("/UserSkill/Cari", userskill.Cari).Methods("GET")
	router.HandleFunc("/UserSkill/Hapus", userskill.Hapus).Methods("GET")
	router.HandleFunc("/UserSkill/Tambah", userskill.Tambah).Methods("POST")

	//Otentifikasi Ability
	router.HandleFunc("/Ability/TambahAbility", ability.TambahAbility).Methods("POST")
	router.HandleFunc("/Ability/TambahAbilityCMD", ability.TambahAbilityCMD).Methods("POST")
	router.HandleFunc("/Ability/AmbilAbility", ability.AmbilAbility).Methods("GET")
	router.HandleFunc("/Ability/AmbilAbilityCMD", ability.AmbilAbilityCMD).Methods("GET")
	router.HandleFunc("/Ability/AmbilAbilityKategori", ability.AmbilAbilityKategori).Methods("GET")
	router.HandleFunc("/Ability/GantiAbility", ability.GantiAbility).Methods("POST")
	router.HandleFunc("/Ability/GantiAbilityCMD", ability.GantiAbilityCMD).Methods("POST")
	router.HandleFunc("/Ability/HapusAbilityCMD", ability.HapusAbilityCMD).Methods("GET")

	//otentifikasi Skill
	router.HandleFunc("/Skill/Tambah", Skill.Tambah).Methods("POST")
	router.HandleFunc("/Skill/TambahSkillCommand", Skill.TambahSkillCommand).Methods("POST")
	router.HandleFunc("/Skill/TambahSkillDeskripsi", Skill.TambahSkillDeskripsi).Methods("POST")
	router.HandleFunc("/Skill/TambahSkillKategori", Skill.TambahSkillKategori).Methods("POST")
	router.HandleFunc("/Skill/TambahSkillReview", Skill.TambahSkillReview).Methods("POST")
	router.HandleFunc("/Skill/Ambil", Skill.Ambil).Methods("GET")
	router.HandleFunc("/Skill/AmbilByKategori", Skill.AmbilByKategori).Methods("GET")
	router.HandleFunc("/Skill/AmbilAuthByEmail", Skill.AmbilAuthByEmail).Methods("GET")
	router.HandleFunc("/Skill/AmbilAuthBySS", Skill.AmbilAuthBySS).Methods("GET")
	router.HandleFunc("/Skill/AmbilAuthSkill", Skill.AmbilAuthSkill).Methods("GET")
	router.HandleFunc("/Skill/AmbilDiscoverSkill", Skill.AmbilDiscoverSkill).Methods("GET")
	router.HandleFunc("/Skill/AmbilSkillKategori", Skill.AmbilSkillKategori).Methods("GET")
	router.HandleFunc("/Skill/AmbilReviewSkill", Skill.AmbilReviewSkill).Methods("GET")
	router.HandleFunc("/Skill/AmbilRatingSkill", Skill.AmbilRatingSkill).Methods("GET")
	router.HandleFunc("/Skill/AmbilCommandSkill", Skill.AmbilCommandSkill).Methods("GET")
	router.HandleFunc("/Skill/AmbilCommandSkillbyTopic", Skill.AmbilCommandSkillbyTopic).Methods("GET")
	router.HandleFunc("/Skill/AmbilSkillByNama", Skill.AmbilSkillByNama).Methods("GET")
	router.HandleFunc("/Skill/GantiStatusSkill", Skill.GantiStatusSkill).Methods("POST")
	router.HandleFunc("/Skill/HapusSkillCommand", Skill.HapusSkillCommand).Methods("GET")
	router.HandleFunc("/Skill/HapusSkill", Skill.HapusSkill).Methods("GET")
	router.HandleFunc("/Skill/GantiSkillKategori", Skill.GantiSkillKategori).Methods("POST")
	router.HandleFunc("/Skill/GantiSkillCommand", Skill.GantiSkillCommand).Methods("POST")
	router.HandleFunc("/Skill/GantiSkillDeskripsi", Skill.GantiSkillDeskripsi).Methods("POST")
	//router.HandleFunc("/Skill/AmbilAuthBySS", Skill.AmbilAuthBySS).Methods("GET")
	//router.HandleFunc("/Skill/AmbilAuthBySS", Skill.AmbilAuthBySS).Methods("GET")

	//http.Handle("/", router)
	fmt.Println("Connected to port 13001")
	// log.Fatal(http.ListenAndServe(":13001", router))
	log.Fatal(http.ListenAndServe(":13001", handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(router)))

}
