package models

import "github.com/dgrijalva/jwt-go"

var JwtKey = []byte("kunci_bahasa_kita")
var errq error

type Users struct {
	AlamatSurel  string `form:"alamat_surel" json:"alamat_surel"`
	NamaPengguna string `form:"nama_pengguna" json:"nama_pengguna"`
	KataSandi    string `form:"kata_sandi" json:"kata_sandi"`
	UID          string `form:"uid" json:"uid"`
	Status       int    `form:"status" json:status`
	Otoritas     int    `form:"otoritas" json:"otoritas"`
	Jumlah       int    `form:"jumlah" json:"jumlah"`
}
type Tag struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Uid      string `json:"uid"`
	Otoritas int    `json:"otoritas"`
}
type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    Tag
}
type Claims struct {
	Pengguna []byte
	Surel    []byte
	Sandi    []byte
	jwt.StandardClaims
}
