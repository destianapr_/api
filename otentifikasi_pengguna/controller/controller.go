package controller

import (
	"SmartSpeaker/Api/database"
	"SmartSpeaker/Api/otentifikasi_pengguna/models"
	"database/sql"
	"encoding/json"
	_ "fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

var Variabel string = "192.168.100.54" // "http://10.226.174.128:13001"

func HomeLink(w http.ResponseWriter, r *http.Request) {
	var response models.Response
	response.Status = 1
	response.Message = "Hai, Selamat Datang!!"
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Daftar(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	var users2 models.Users
	var response models.Response
	//var mail models.Mail
	db := database.Connect()
	defer db.Close()
	ValueNamaPengguna := r.FormValue("nama_pengguna")
	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueKataSandi := r.FormValue("kata_sandi")
	//query := db.QueryRow("SELECT count(if(email=?, 1, null)) as jmlh_email, count(if(username=?, 1, null)) as jmlh_username FROM user", ValueAlamatSurel, ValueNamaPengguna).Scan(&users.Jumlah, &users2.Jumlah)
	query, users, users2 := database.SelectDaftar(ValueAlamatSurel, ValueNamaPengguna)
	switch {
	case (query != sql.ErrNoRows) && (users.Jumlah == 0) && (users2.Jumlah == 0):
		hashedUsername, _ := bcrypt.GenerateFromPassword([]byte(ValueNamaPengguna), bcrypt.DefaultCost)
		hashedEmail, _ := bcrypt.GenerateFromPassword([]byte(ValueAlamatSurel), bcrypt.DefaultCost)
		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(ValueKataSandi), bcrypt.DefaultCost)
		hashedJWTKey, _ := bcrypt.GenerateFromPassword([]byte(models.JwtKey), bcrypt.DefaultCost)
		claims := &models.Claims{
			Pengguna: hashedUsername,
			Surel:    hashedEmail,
			Sandi:    hashedPassword,
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenString, _ := token.SignedString(hashedJWTKey)
		_, query := db.Exec("INSERT INTO user (email, username, password, uid) values (?,?,?,?)", ValueAlamatSurel, ValueNamaPengguna, hashedPassword, tokenString)
		//query := database.InsertDaftar(ValueAlamatSurel, ValueNamaPengguna, hashedPassword, tokenString)
		if query == nil {
			response.Status = 0
			response.Message = "Berhasil Menambah Data"
			log.Print("Menambah data ke database")
			to := []string{ValueAlamatSurel}
			cc := []string{"anggityn@bahasakita.co.id"}
			subject := "API E-Mail BahasaKita"
			message := ("Verifikasi akun anda dengan klik link ini ---> " + Variabel + "/User/" + tokenString)
			go sendMail(to, cc, subject, message)
		}
	case (query != sql.ErrNoRows) && (users.Jumlah == 1) && (users2.Jumlah == 0):
		response.Status = 1
		response.Message = "alamat surel sudah ada"
		log.Print("alamat surel sudah ada")
	case (query != sql.ErrNoRows) && (users.Jumlah == 0) && (users2.Jumlah == 1):
		response.Status = 2
		response.Message = "nama pengguna sudah ada"
		log.Print("nama pengguna sudah ada")
	case (query != sql.ErrNoRows) && (users.Jumlah == 1) && (users2.Jumlah == 1):
		response.Status = 3
		response.Message = "alamat surel dan nama pengguna sudah ada"
		log.Print("alamat surel dan nama pengguna sudah ada")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func VerifikasiUlang(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	var response models.Response
	//var claims models.Claims
	db := database.Connect()
	defer db.Close()
	ValueAlamatSurel := r.FormValue("alamat_surel")
	//query := db.QueryRow("SELECT COUNT(email) as jmlh, uid, status FROM user WHERE email=?", ValueAlamatSurel).Scan(&users.Jumlah, &users.UID, &users.Status)
	query, users := database.SelectVerifUlang(ValueAlamatSurel)
	switch {
	case (query != sql.ErrNoRows) && (users.Jumlah == 1) && (users.Status == 0):
		response.Status = 0
		response.Message = "Berhasil Menambah Data"
		log.Print("Menambah data ke database")
		to := []string{ValueAlamatSurel}
		cc := []string{"anggityn@bahasakita.co.id"}
		subject := "API E-Mail BahasaKita"
		message := ("Verifikasi akun anda dengan klik link ini ---> " + Variabel + "/User" + users.UID)
		//kirimEmail :=
		go sendMail(to, cc, subject, message)
		//if kirimEmail != nil {
		//	log.Fatal(kirimEmail.Error())
		//}
	case (query != sql.ErrNoRows) && (users.Jumlah == 1) && (users.Status == 1):
		response.Status = 1
		response.Message = "pengguna telah verifikasi"
		log.Print("pengguna telah verifikasi")
	default:
		response.Status = 2
		response.Message = "alamat surel belum terdaftar"
		log.Print("alamat surel belum terdaftar")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Verifikasi(w http.ResponseWriter, r *http.Request) {
	var response models.Response
	db := database.Connect()
	defer db.Close()
	//_, err := db.Exec("UPDATE user SET status = 1 WHERE uid = ?", mux.Vars(r)["uid"])
	err := database.UpdateVerif(mux.Vars(r)["uid"])
	switch {
	case err != nil:
		response.Status = 0
		response.Message = "Gagal Verifikasi"
		log.Print("Gagal Update Status Verifikasi")
	default:
		response.Status = 1
		response.Message = "User Berhasil Verifikasi"
		log.Print("Verifikasi User Berhasil")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func Login(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	var response models.Response
	var tag models.Tag
	db := database.Connect()
	defer db.Close()
	ValueNamaPengguna := r.FormValue("alamat_surel")
	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueKataSandi := r.FormValue("kata_sandi")
	//row := db.QueryRow("SELECT count(email) as jumlah , (COALESCE(otoritas, 0)) as otoritas, (COALESCE(status, 0)) as status, (COALESCE(uid, 0)) as uid, (COALESCE(email, '-')) as email, (COALESCE(username, '-')) as username, (COALESCE(password, '-')) as password FROM user WHERE (username=? or email=?)", ValueNamaPengguna, ValueAlamatSurel).Scan(&users.Jumlah, &tag.Otoritas, &users.Status, &tag.Uid, &tag.Email, &tag.Username, &users.KataSandi)
	row, users, tag := database.SelectLogin(ValueNamaPengguna, ValueAlamatSurel)
	if row == nil && users.Jumlah == 0 {
		response.Status = 1
		response.Message = "nama_pengguna atau alamat_surel belum terdaftar"
		log.Print("nama_pengguna atau alamat_surel belum terdaftar")
	} else {
		compareSandi := bcrypt.CompareHashAndPassword([]byte(users.KataSandi), []byte(ValueKataSandi))
		if compareSandi != nil {
			response.Status = 2
			response.Message = "kata_sandi tidak ditemukan"
			log.Print("kata_sandi tidak ditemukan")
		} else if users.Jumlah == 1 && users.Status == 0 {
			response.Status = 3
			response.Message = "pengguna belum melakukan verifikasi"
			log.Print("pengguna belum melakukan verifikasi")
		} else if users.Jumlah == 1 && users.Status == 1 {
			response.Status = 0
			response.Message = "pengguna berhasil masuk"
			response.Data = tag
			log.Print("berhasil masuk!")
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func LupaSandi(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	var response models.Response
	db := database.Connect()
	defer db.Close()
	ValueAlamatSurel := r.FormValue("alamat_surel")
	//query := db.QueryRow("SELECT COUNT(email) as jmlh FROM user WHERE email=?", ValueAlamatSurel).Scan(&users.Jumlah)
	query, users := database.SelectLupaSandi(ValueAlamatSurel)
	switch {
	case (query != sql.ErrNoRows) && (users.Jumlah == 1):
		randPass := RandomPassword(10)
		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(randPass), bcrypt.DefaultCost)
		//_, err := db.Exec("UPDATE user SET password = ? WHERE email = ?", hashedPassword, ValueAlamatSurel)
		err := database.UpdateLupaSandi(hashedPassword, ValueAlamatSurel)
		if err == nil {
			response.Status = 1
			response.Message = "Berhasil Reset Sandi"
			log.Print("Berhasil Reset Sandi")
			// kirim sandi sementara ke akun surel pengguna
			to := []string{ValueAlamatSurel}
			cc := []string{"anggityn@bahasakita.co.id"}
			subject := "API E-Mail BahasaKita"
			message := ("Silahkan masuk aplikasi dengan menggunakan sandi ini \n\nsandi : " + randPass + "\n\nJangan berikan sandi ini ke siapapun kecuali anda. Anda dapat mengganti sandi dengan pilih menu Pengaturan >> Atur Ulang Sandi")
			go sendMail(to, cc, subject, message)
		}
	default:
		response.Status = 0
		response.Message = "alamat_surel belum terdaftar"
		log.Print("alamat_surel belum terdaftar")
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func AturSandi(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	var response models.Response
	db := database.Connect()
	defer db.Close()
	ValueNamaPengguna := r.FormValue("alamat_surel")
	ValueAlamatSurel := r.FormValue("alamat_surel")
	ValueKataSandi := r.FormValue("kata_sandi")
	ValueKataSandiBaru := r.FormValue("kata_sandi_baru")
	//query := db.QueryRow("SELECT COUNT(email or username) as jmlh, password FROM user WHERE email=? or username=?", ValueAlamatSurel, ValueNamaPengguna).Scan(&users.Jumlah, &users.KataSandi)
	query, users := database.SelectAturSandi(ValueAlamatSurel, ValueNamaPengguna)
	switch {
	case (query != sql.ErrNoRows) && (users.Jumlah == 0):
		response.Status = 1
		response.Message = "nama_pengguna atau alamat_surel belum terdaftar"
		log.Print("nama_pengguna atau alamat_surel belum terdaftar")
	case (query != sql.ErrNoRows) && (users.Jumlah == 1):
		compareSandi := bcrypt.CompareHashAndPassword([]byte(users.KataSandi), []byte(ValueKataSandi))
		if compareSandi != nil {
			response.Status = 2
			response.Message = "kata_sandi tidak ditemukan"
			log.Print("kata_sandi tidak ditemukan")
		} else if compareSandi == nil {
			//ganti password
			hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(ValueKataSandiBaru), bcrypt.DefaultCost)
			//_, errx := db.Exec("UPDATE user SET password = ? WHERE email = ? or username = ?", hashedPassword, ValueAlamatSurel, ValueNamaPengguna)
			errx := database.UpdateAturSandi(hashedPassword, ValueAlamatSurel, ValueNamaPengguna)
			if errx == nil {
				response.Status = 0
				response.Message = "Berhasil Ganti Kata Sandi"
				log.Print("Berhasil Ganti Kata Sandi")
			}
		}
	default:
		log.Print(query)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func RandomPassword(num int) string {
	rand.Seed(time.Now().UnixNano())
	length := num
	var b strings.Builder
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789" + "-_")
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String() // E.g. "Acv1Mded0o"
	log.Println("sandi: " + str)
	return str
}
